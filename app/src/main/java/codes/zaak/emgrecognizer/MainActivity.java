package codes.zaak.emgrecognizer;


import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;


import com.parrot.arsdk.ARSDK;

import java.util.List;

import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoController;
import codes.zaak.myorecognizer.MyoDiscovery;
import codes.zaak.myorecognizer.MyoGattCallback;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.commands.MyoCommands;
import codes.zaak.myorecognizer.communication.MyoCommunicator;
import codes.zaak.myorecognizer.gestures.CustomGestureController;
import codes.zaak.myorecognizer.model.Accelerometer;
import codes.zaak.myorecognizer.model.Gyroscope;
import codes.zaak.myorecognizer.model.Myo;
import codes.zaak.myorecognizer.model.Orientation;
import codes.zaak.myorecognizer.processor.classifier.ClassifierEvent;
import codes.zaak.myorecognizer.processor.classifier.ClassifierProcessor;
import codes.zaak.myorecognizer.processor.emg.EmgData;
import codes.zaak.myorecognizer.processor.emg.EmgProcessor;
import codes.zaak.myorecognizer.processor.imu.ImuData;
import codes.zaak.myorecognizer.processor.imu.ImuProcessor;
import codes.zaak.myorecognizer.processor.imu.MotionEvent;
import codes.zaak.myorecognizer.processor.imu.MotionProcessor;
import codes.zaak.myorecognizer.gestures.GestureDetect;
import codes.zaak.myorecognizer.gestures.GestureSave;


public class MainActivity extends AppCompatActivity implements MyoListener.ConnectionListener, MyoListener.ReadMyoInfoCallback, MyoListener.BatteryCallback, MyoListener.ImuDataListener, MyoListener.MotionEventListener, MyoListener.ClassifierEventListener, MyoListener.CustomGesturesListener {


    private TextView status;
    private TextView accelerator;
    private TextView orientation;
    private TextView gyroscope;

    private MyoDiscovery myoDiscovery;


    private ClassifierProcessor classifierProcessor;
    private ImuProcessor imuProcessor;
    private MotionProcessor motionProcessor;

    private MyoController myoController;
    private CustomGestureController customGestureController;

    Handler handler;

    private boolean isRecording = false;
    private boolean isDetecting = false;
    private int detected = 0;
    private int failed = 0;


    private long lastUpdate = 0;

    static {
        ARSDK.loadSDKLibs();
    }

    private void setStatus(final String message) {
        if (handler != null)
            handler.post(new Runnable() {
                @Override
                public void run() {
                    status.setText(message);
                }
            });
    }

    private void setAccelerator(final String message) {
        if (handler != null)
            handler.post(new Runnable() {
                @Override
                public void run() {
                    accelerator.setText(message);
                }
            });

    }

    private void setOrientation(final String message) {
        if (handler != null)
            handler.post(new Runnable() {
                @Override
                public void run() {
                    orientation.setText(message);
                }
            });

    }

    private void setGyroscope(final String message) {
        if (handler != null)
            handler.post(new Runnable() {
                @Override
                public void run() {
                    gyroscope.setText(message);
                }
            });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        myoDiscovery = new MyoDiscovery(this);
        handler = new Handler();

        // bleController = new BluetoothLeController(this);
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        status = (TextView) findViewById(R.id.myo_status);
        accelerator = (TextView) findViewById(R.id.myo_accelerator);
        orientation = (TextView) findViewById(R.id.myo_orientation);
        gyroscope = (TextView) findViewById(R.id.myo_gyroscope);

        Button record = (Button) findViewById(R.id.button);
        Button detect = (Button) findViewById(R.id.button2);
        Button clear = (Button) findViewById(R.id.button3);


        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customGestureController.setGestureStatus(null, MyoStates.GestureStatus.RECORDING);
            }
        });

        detect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customGestureController.setGestureStatus(null, MyoStates.GestureStatus.DETECTING);
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customGestureController.setGestureStatus(null, MyoStates.GestureStatus.CLEAR);
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myoController.writeVibrate(MyoCommands.VibrateType.LONG, null);
            }
        });

        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                return true;
            }
        });
    }

    private MyoListener.ScannerCallback scannerCallback = new MyoListener.ScannerCallback() {
        @Override
        public void onScanFinished(List<MyoController> myoList) {
            Log.d("ScanResult", "Result: " + myoList.size());

            for (MyoController myoController : myoList) {

                myoController.addConnectionListener(MainActivity.this);
                myoController.connect();
                myoController.setConnectionSpeed(MyoStates.ConnectionSpeed.HIGH);
                myoController.writeSleepMode(MyoCommands.SleepMode.NEVER, null);
                myoController.writeMode(MyoCommands.EmgMode.FILTERED, MyoCommands.ImuMode.NONE, MyoCommands.ClassifierMode.DISABLED, null);
                myoController.writeUnlock(MyoCommands.UnlockType.HOLD, null);
                MainActivity.this.myoController = myoController;


            }
        }

        @Override
        public void onScanFailed(MyoStates.ScanError error) {

        }
    };

    @Override
    protected void onDestroy() {
        if (this.myoController != null) {
            this.myoController.writeSleepMode(MyoCommands.SleepMode.NORMAL, null);
            myoController.writeMode(MyoCommands.EmgMode.NONE, MyoCommands.ImuMode.NONE, MyoCommands.ClassifierMode.DISABLED, null);
            this.myoController.disconnect();

        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_connect) {
            myoDiscovery.startDiscover(1000L, scannerCallback);
        }

        if (id == R.id.action_disconnect) {
            if (this.myoController != null) {
                this.myoController.disconnect();
                this.myoController.writeSleepMode(MyoCommands.SleepMode.NORMAL, null);
            }
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onConnectionStateChanged(MyoGattCallback myoCallback, MyoStates.ConnectionState state) {
        setStatus(state.name());
    }

    @Override
    public void onBatteryLevelRead(MyoController myoController, MyoCommunicator communicator, int batteryLevel) {
        setGyroscope("BatteryLevel: " + batteryLevel);
    }


    @Override
    public void onMotionEvent(MotionEvent motionEvent) {
        setAccelerator(motionEvent.getType().toString());
    }

    @Override
    public void onClassifierEvent(ClassifierEvent classifierEvent) {
        setStatus(classifierEvent.getType().name());
    }

    @Override
    public void onGestureDetected(long timeStamp, Gestures.CustomGestures gesture) {
        setAccelerator(gesture.name());
    }

    @Override
    public void onGestureProfileLoaded(long timeStamp, Gestures.ProfileStatus status, int gestureCount) {

    }

    @Override
    public void onGestureRecordingStatusChanged(long timeStamp, Gestures.CustomGestures currentGesture, Gestures.GestureSaveStatus status) {
        switch (status) {
            case SUCCESS: {
                setStatus(status.name());

                break;
            }
            case ERROR: {
                setStatus(status.name());
                break;
            }
            case SAVING: {
                setStatus(status.name());
                break;
            }
            case READY: {
                setStatus(status.name());
                break;
            }
        }
    }

    @Override
    public void onGestureStored(long timeStamp, Gestures.CustomGestures savedGesture, Gestures.GestureSaveStatus success) {
        if (success == Gestures.GestureSaveStatus.SUCCESS) {
            setOrientation(savedGesture.name());
            customGestureController.setGestureStatus(null, MyoStates.GestureStatus.DETECTING);
        }
    }

    @Override
    public void onCheckGestureDistance(long l, Gestures.CustomGestures gesture, Gestures.CustomGestures gestureToStore, Gestures.GestureSaveStatus error) {

    }

    @Override
    public void onReadMyoInfo(MyoController myoController, MyoCommunicator msg, Myo myo) {

    }

    @Override
    public void onDeviceNameRead(MyoController myoController, MyoCommunicator communicator, String deviceName) {

    }

    @Override
    public void onManufacturerNameRead(MyoController myoController, MyoCommunicator communicator, String manufacturer) {

    }

    @Override
    public void onFirmwareRead(MyoController myoController, MyoCommunicator communicator, String version) {

    }

    @Override
    public void onOrientationChanged(Orientation orientationData) {

    }

    @Override
    public void onAccelerationChanged(Accelerometer accelerometerData) {

    }

    @Override
    public void onGyroscopeChanged(Gyroscope gyroscopeData) {

    }
}
