package codes.zaak.emgrecognizer;

import android.app.Application;
import android.content.Context;

/**
 * Created by user on 21.04.16.
 */
public class App extends Application {

    private static App mInstance;
    private static Context context;

    public void onCreate() {
        super.onCreate();
        mInstance = this;

        context = getApplicationContext();


    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return context;
    }
}
