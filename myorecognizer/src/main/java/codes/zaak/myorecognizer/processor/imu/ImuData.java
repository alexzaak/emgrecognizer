package codes.zaak.myorecognizer.processor.imu;

import codes.zaak.myorecognizer.model.Accelerometer;
import codes.zaak.myorecognizer.model.Gyroscope;
import codes.zaak.myorecognizer.model.Orientation;
import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.processor.DataPacket;
import codes.zaak.myorecognizer.utils.ByteReader;

/**
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 *
 * Class representing data from the Myo's IMU sensors.
 */
public class ImuData extends DataPacket {
    private static final double MYOHW_ORIENTATION_SCALE = 16384.0f; ///< See myohw_imu_data_t::orientation
    private static final double MYOHW_ACCELEROMETER_SCALE = 2048.0f; ///< See myohw_imu_data_t::accelerometer
    private static final double MYOHW_ACCEMYOHW_GYROSCOPE_SCALELEROMETER_SCALE = 16.0f; ///< See myohw_imu_data_t::gyroscope

    private final Orientation orientation;
    private final Accelerometer accelerometer;
    private final Gyroscope gyroscope;


    public ImuData(BaseDataPacket packet) {
        super(packet.getDeviceAddress(), packet.getTimeStamp());
        ByteReader byteHelper = new ByteReader(packet.getData());
        double[] mOrientationData = new double[4];
        for (int i = 0; i < 4; i++) {
            mOrientationData[i] = byteHelper.getUInt16() / MYOHW_ORIENTATION_SCALE;


        }
        orientation = new Orientation(mOrientationData[0], mOrientationData[1], mOrientationData[2], mOrientationData[3]);

        double[] mAccelerometerData = new double[3];
        for (int i = 0; i < 3; i++) {
            mAccelerometerData[i] = byteHelper.getUInt16() / MYOHW_ACCELEROMETER_SCALE;
        }
        accelerometer = new Accelerometer(mAccelerometerData[0], mAccelerometerData[1], mAccelerometerData[2]);

        double[] mGyroData = new double[3];
        for (int i = 0; i < 3; i++)
            mGyroData[i] = byteHelper.getUInt16() / MYOHW_ACCEMYOHW_GYROSCOPE_SCALELEROMETER_SCALE;

        gyroscope = new Gyroscope(mGyroData[0], mGyroData[1], mGyroData[2]);
    }


    /**
     * Values range form -1.0 to 1.0<br>
     * Format: [w,x,y,z]
     *
     * @see <a href="https://github.com/thalmiclabs/myo-bluetooth/blob/master/myohw.h">Myo protocol specification</a>
     */
    public Orientation getOrientationData() {
        return orientation;
    }

    /**
     * Values range from -1.0 to 1.0 <br>
     * Format: [?,?,?]
     *
     * @see <a href="https://github.com/thalmiclabs/myo-bluetooth/blob/master/myohw.h">Myo protocol specification</a>
     */
    public Accelerometer getAccelerometerData() {
        return accelerometer;
    }

    /**
     * Values range from -? to ? <br>
     * Format: [?,?,?]
     *
     * @see <a href="https://github.com/thalmiclabs/myo-bluetooth/blob/master/myohw.h">Myo protocol specification</a>
     */
    public Gyroscope getGyroData() {
        return gyroscope;
    }

    public static String format(double[] data) {
        StringBuilder builder = new StringBuilder();
        for (double d : data)
            builder.append(String.format("%+.3f", d)).append(" ");
        return builder.toString();
    }

}