package codes.zaak.myorecognizer.processor.imu;

import android.util.Log;

import java.util.Arrays;

import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.processor.BaseProcessor;
import codes.zaak.myorecognizer.services.ImuService;
import codes.zaak.myorecognizer.utils.ByteReader;

/**
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 *
 * @see <a href="https://github.com/thalmiclabs/myo-bluetooth/blob/master/myohw.h#L301">Myo Bluetooth Protocol</a>
 */
public class MotionProcessor extends BaseProcessor {
    private static final String TAG = "MyoLib:MotionProcessor";

    public MotionProcessor() {
        super();
        getSubscriptions().add(ImuService.MOTION_EVENT.getCharacteristicUUID());
    }

    @Override
    protected void doProcess(BaseDataPacket packet) {
        Log.v(TAG, Arrays.toString(packet.getData()));

        ByteReader byteHelper = new ByteReader(packet.getData());
        MotionEvent event = null;
        int typeValue = byteHelper.getUInt8();
        if (typeValue == MotionEvent.Type.TAP.getValue()) {
            TapMotionEvent _event = new TapMotionEvent(packet);
            // TODO possible values are unknown
            int tapDirectionValue = byteHelper.getUInt8();
            int tapCountValue = byteHelper.getUInt8();
            _event.setTapCount(tapCountValue);
            event = _event;
        }
        if (event == null) {
            Log.e(TAG, "Unknown motion event type!");
            return;
        }


        for (DataListener listener : getDataListeners()) {
            MyoListener.MotionEventListener motionEventListener = (MyoListener.MotionEventListener) listener;
            motionEventListener.onMotionEvent(event);
        }
    }

    public void addListener(MyoListener.MotionEventListener listener) {
        super.addDataListener(listener);
    }


}
