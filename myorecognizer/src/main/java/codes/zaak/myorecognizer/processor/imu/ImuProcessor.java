package codes.zaak.myorecognizer.processor.imu;

import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.processor.BaseProcessor;
import codes.zaak.myorecognizer.services.ImuService;

/**
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 *
 * Created by user on 24.04.16.
 * Processor that converts {@link BaseDataPacket} object into {@link ImuData} objects.
 */
public class ImuProcessor extends BaseProcessor {
    private static final String TAG = "MyoLib:ImuProcessor";

    public ImuProcessor() {
        super();
        getSubscriptions().add(ImuService.IMU_DATA.getCharacteristicUUID());
    }

    @Override
    protected void doProcess(BaseDataPacket packet) {
        ImuData imuData = new ImuData(packet);

        for (DataListener listener : getDataListeners()) {
            MyoListener.ImuDataListener imuListener = (MyoListener.ImuDataListener) listener;

            imuListener.onAccelerationChanged(imuData.getAccelerometerData());
            imuListener.onOrientationChanged(imuData.getOrientationData());
            imuListener.onGyroscopeChanged(imuData.getGyroData());

        }
    }


    public void addListener(MyoListener.ImuDataListener listener) {
        super.addDataListener(listener);
    }
}