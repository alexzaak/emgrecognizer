package codes.zaak.myorecognizer.processor.classifier;

import android.util.Log;

import java.util.Arrays;

import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.processor.BaseProcessor;
import codes.zaak.myorecognizer.services.ClassifierService;
import codes.zaak.myorecognizer.utils.ByteReader;

/**
 * @author Kutafina E, Laukamp D, Jonas SM.
 *
 * Wearable Sensors in Medical Education: Supporting Hand Hygiene Training with a Forearm EMG. Stud Health Technol Inform. 2015;211:286-91.
 * @see <a href="https://github.com/thalmiclabs/myo-bluetooth/blob/master/myohw.h#L345">Myo Bluetooth Protocol</a>
 */
public class ClassifierProcessor extends BaseProcessor {
    private static final String TAG = "ClassifierProcessor";

    public ClassifierProcessor() {
        super();
        getSubscriptions().add(ClassifierService.CLASSIFIER_EVENT.getCharacteristicUUID());
    }

    @Override
    protected void doProcess(BaseDataPacket packet) {
        Log.v(TAG, Arrays.toString(packet.getData()));

        ByteReader byteHelper = new ByteReader(packet.getData());
        int typeValue = byteHelper.getUInt8();
        ClassifierEvent event = null;

        if (typeValue == ClassifierEvent.Type.ARM_SYNCED.getValue()) {
            event = new ArmSyncedClassifierEvent(packet);
        } else if (typeValue == ClassifierEvent.Type.ARM_UNSYNCED.getValue()) {
            event = new ClassifierEvent(packet, ClassifierEvent.Type.ARM_UNSYNCED);
        } else if (typeValue == ClassifierEvent.Type.POSE.getValue()) {
            event = new PoseClassifierEvent(packet);
        } else if (typeValue == ClassifierEvent.Type.UNLOCKED.getValue()) {
            event = new ClassifierEvent(packet, ClassifierEvent.Type.UNLOCKED);
        } else if (typeValue == ClassifierEvent.Type.LOCKED.getValue()) {
            event = new ClassifierEvent(packet, ClassifierEvent.Type.LOCKED);
        } else if (typeValue == ClassifierEvent.Type.SYNC_FAILED.getValue()) {
            event = new SyncFailedClassifierEvent(packet);
        } else if (typeValue == ClassifierEvent.Type.WARM_UP_RESULT.getValue()) {
            event = new WarmUpResultClassifierEvent(packet);
        }
        if (event == null) {
            Log.e(TAG, "Unknown classifier event type!");
            return;
        }
        for (DataListener listener : getDataListeners()) {
            MyoListener.ClassifierEventListener motionEventListener = (MyoListener.ClassifierEventListener) listener;
            motionEventListener.onClassifierEvent(event);
        }
    }



    public void addListener(MyoListener.ClassifierEventListener listener) {
        super.addDataListener(listener);
    }
}