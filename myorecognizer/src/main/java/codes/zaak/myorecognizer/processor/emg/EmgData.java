package codes.zaak.myorecognizer.processor.emg;

import android.gesture.Gesture;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.utils.ByteReader;

/*
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 *
 *
 * Class to hold EMG data from one of the Myo's 8 EMG sensors.
 */
public class EmgData {


    private Gestures.CustomGestures mGesture = Gestures.CustomGestures.UNDEFINED;
    private final byte[] mData;
    private final long mTimestamp;
    private final String mDeviceAddress;

    private List<Double> emgDataList = new ArrayList<>();



    public EmgData(Gestures.CustomGestures gesture, String deviceAddress, long timestamp, byte[] data) {
        this.mGesture = gesture;
        mDeviceAddress = deviceAddress;
        mTimestamp = timestamp;
        mData = data;
    }

    public String getDeviceAddress() {
        return mDeviceAddress;
    }

    /**
     * @return Array of byte values ranging from -128 to 127.
     * @see <a href="https://github.com/thalmiclabs/myo-bluetooth/blob/master/myohw.h#L371">Myo protocol specification</a>
     */
    public byte[] getData() {
        return mData;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int b : mData)
            builder.append(String.format("%+04d", b)).append(" ");
        return builder.toString();
    }

    public Gestures.CustomGestures getGesture() {
        return mGesture;
    }

    public int getId() {
        return this.mGesture.ordinal();
    }

    public String getName() {
        return this.mGesture.name();
    }

    public String getLine() {
        StringBuilder return_SB = new StringBuilder();
        for (int i_emg_num = 0; i_emg_num < 8; i_emg_num++) {
            return_SB.append(String.format(Locale.US,"%f,", emgDataList.get(i_emg_num)));
        }
        return return_SB.toString();
    }

    public void setLine(String line) {

        if(line == null) {
            return;
        }

        ArrayList<Double> data = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(line , ",");
        for (int i_emg_num = 0; i_emg_num < 8; i_emg_num++) {

            data.add(Double.parseDouble(st.nextToken()));
        }

        emgDataList = data;
        Log.i("GestureStored", getLine());
    }

    public void addEmgData(double emg) {
        emgDataList.add(emg);
    }

    public void setEmgData(int index, double emg) {
        emgDataList.set(index, emg);
    }

    public Double getEmgData(int index) {
        if (index < 0 || index > emgDataList.size() - 1) {
            return null;
        } else {
            return emgDataList.get(index);
        }
    }

    public List<Double> getEmgArray() {
        return this.emgDataList;
    }

    //euklidischer Abstand
    public Double getDistanceFrom(EmgData currentData) {
        Double distance = 0.00;
        for (int index = 0; index < 8; index++) {
            distance += Math.pow((emgDataList.get(index) - currentData.getEmgData(index)), 2.0);
        }
        return Math.sqrt(distance);
    }

    // euclidic inner production
    public Double getInnerProductionTo(EmgData currentData) {
        Double val = 0.00;
        for (int index = 0; index < 8; index++) {
            val += emgDataList.get(index) * currentData.getEmgData(index);
        }
        return val;
    }

    // euclidic Norm
    public Double getNorm() {
        Double norm = 0.00;
        for (int index = 0; index < 8; index++) {
            norm += Math.pow(emgDataList.get(index), 2.0);
        }
        return Math.sqrt(norm);
    }



    /**
     * Timestamp of this EMG dataset. Based on {@link BaseDataPacket#getTimeStamp()}.
     * As every {@link BaseDataPacket} contains two EMG datasets.
     * The second EMG data step will have {@link BaseDataPacket#getTimeStamp()}+5ms as timestamp.
     *
     * @return timestamp in miliseconds
     * @see <a href="http://developerblog.myo.com/myocraft-emg-in-the-bluetooth-protocol/">Myo blog: EMG data</a>
     */
    public long getTimestamp() {
        return mTimestamp;
    }
}