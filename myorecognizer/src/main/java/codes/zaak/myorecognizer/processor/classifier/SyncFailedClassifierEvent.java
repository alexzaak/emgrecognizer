package codes.zaak.myorecognizer.processor.classifier;

import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.utils.ByteReader;

/**
 * @author  Kutafina E, Laukamp D, Jonas SM.
 *
 * Wearable Sensors in Medical Education: Supporting Hand Hygiene Training with a Forearm EMG. Stud Health Technol Inform. 2015;211:286-91.
 */
public class SyncFailedClassifierEvent extends ClassifierEvent {
    /**
     * Possible outcomes when the user attempts a sync gesture.
     */
    public enum SyncResult {
        /**
         * Sync gesture was performed too hard.
         */
        FAILED_TOO_HARD((byte) 0x01);

        private final byte mValue;

        SyncResult(byte value) {
            mValue = value;
        }

        public byte getValue() {
            return mValue;
        }
    }

    private SyncResult mSyncResult;

    public SyncFailedClassifierEvent(BaseDataPacket packet) {
        super(packet, Type.SYNC_FAILED);
        ByteReader byteHelper = new ByteReader(packet.getData());
        int typeValue = byteHelper.getUInt8();
        if (getType().getValue() != typeValue)
            throw new RuntimeException("Incompatible BaseDataPacket:" + typeValue);

        int syncResultValue = byteHelper.getUInt8();
        for (SyncFailedClassifierEvent.SyncResult syncResult : SyncFailedClassifierEvent.SyncResult.values()) {
            if (syncResult.getValue() == syncResultValue) {
                mSyncResult = syncResult;
                break;
            }
        }
    }

    public SyncResult getSyncResult() {
        return mSyncResult;
    }

}