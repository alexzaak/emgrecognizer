package codes.zaak.myorecognizer.processor.classifier;

import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.utils.ByteReader;

/**
 * @author  Kutafina E, Laukamp D, Jonas SM.
 *
 * Wearable Sensors in Medical Education: Supporting Hand Hygiene Training with a Forearm EMG. Stud Health Technol Inform. 2015;211:286-91.
 */
public class PoseClassifierEvent extends ClassifierEvent {
    public enum Pose {
        REST((short) 0x0000), FIST((short) 0x0001), WAVE_IN((short) 0x0002), WAVE_OUT((short) 0x0003), FINGERS_SPREAD((short) 0x0004), DOUBLE_TAP((short) 0x0005), UNKNOWN((short) 0xFFFF);

        private final short mValue;

        Pose(short value) {
            mValue = value;
        }

        public short getValue() {
            return mValue;
        }
    }

    private Pose mPose = Pose.UNKNOWN;

    public PoseClassifierEvent(BaseDataPacket packet) {
        super(packet, Type.POSE);
        ByteReader byteHelper = new ByteReader(packet.getData());
        int typeValue = byteHelper.getUInt8();
        if (getType().getValue() != typeValue)
            throw new RuntimeException("Incompatible BaseDataPacket:" + typeValue);

        int poseValue = byteHelper.getUInt16();
        for (PoseClassifierEvent.Pose pose : PoseClassifierEvent.Pose.values()) {
            if (pose.getValue() == poseValue) {
                mPose = pose;
                break;
            }
        }
    }


    public Pose getPose() {
        return mPose;
    }
}