package codes.zaak.myorecognizer.processor.classifier;

import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.utils.ByteReader;

/**
 * @author  Kutafina E, Laukamp D, Jonas SM.
 *
 * Wearable Sensors in Medical Education: Supporting Hand Hygiene Training with a Forearm EMG. Stud Health Technol Inform. 2015;211:286-91.
 */
public class WarmUpResultClassifierEvent extends ClassifierEvent {
    /**
     * Possible warm-up results for Myo.
     */
    public enum WarmUpResult {
        UNKNOWN((byte) 0x00), SUCCESS((byte) 0x01), FAILED_TIMEOUT((byte) 0x02);
        private final byte mValue;

        WarmUpResult(byte value) {
            mValue = value;
        }

        public byte getValue() {
            return mValue;
        }
    }

    private WarmUpResult mWarmUpResult;

    public WarmUpResultClassifierEvent(BaseDataPacket packet) {
        super(packet, Type.WARM_UP_RESULT);
        ByteReader byteHelper = new ByteReader(packet.getData());
        int typeValue = byteHelper.getUInt8();
        if (getType().getValue() != typeValue)
            throw new RuntimeException("Incompatible BaseDataPacket:" + typeValue);

        int warmUpResultValue = byteHelper.getUInt8();
        for (WarmUpResultClassifierEvent.WarmUpResult warmUpResult : WarmUpResultClassifierEvent.WarmUpResult.values()) {
            if (warmUpResult.getValue() == warmUpResultValue) {
                mWarmUpResult = warmUpResult;
                break;
            }
        }
    }

    public WarmUpResult getWarmUpResult() {
        return mWarmUpResult;
    }

    public void setWarmUpResult(WarmUpResult warmUpResult) {
        mWarmUpResult = warmUpResult;
    }
}