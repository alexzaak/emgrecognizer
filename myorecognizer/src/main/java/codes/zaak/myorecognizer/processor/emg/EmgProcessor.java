package codes.zaak.myorecognizer.processor.emg;

import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.processor.BaseProcessor;
import codes.zaak.myorecognizer.services.EmgService;

/**
 /*
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 *
 * Creates {@link EmgData} objects from {@link BaseDataPacket} objects.<br/>
 * More specifically, two for each.
 */
public class EmgProcessor extends BaseProcessor {
    private static final String TAG = "EmgProcessor";

    public EmgProcessor() {
        super();
        getSubscriptions().add(EmgService.EMG_DATA_0.getCharacteristicUUID());
        getSubscriptions().add(EmgService.EMG_DATA_1.getCharacteristicUUID());
        getSubscriptions().add(EmgService.EMG_DATA_2.getCharacteristicUUID());
        getSubscriptions().add(EmgService.EMG_DATA_3.getCharacteristicUUID());
    }

    protected void doProcess(BaseDataPacket packet) {
        byte[] data1 = new byte[8];
        System.arraycopy(packet.getData(), 0, data1, 0, 8);
        EmgData emgData1 = new EmgData(Gestures.CustomGestures.UNDEFINED, packet.getDeviceAddress(), packet.getTimeStamp(), packet.getData());

        byte[] data2 = new byte[8];
        System.arraycopy(packet.getData(), 8, data2, 0, 8);
        EmgData emgData2 = new EmgData(Gestures.CustomGestures.UNDEFINED, packet.getDeviceAddress(), packet.getTimeStamp() + 5, data2);

        for (DataListener listener : getDataListeners()) {
            MyoListener.EmgDataListener emgListener = (MyoListener.EmgDataListener) listener;
            emgListener.onNewEmgData(emgData1);
           // emgListener.onNewEmgData(emgData2);
        }
    }



    public void addListener(MyoListener.EmgDataListener listener) {
        super.addDataListener(listener);
    }
}

