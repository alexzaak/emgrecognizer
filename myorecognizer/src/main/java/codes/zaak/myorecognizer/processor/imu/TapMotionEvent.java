package codes.zaak.myorecognizer.processor.imu;

import codes.zaak.myorecognizer.processor.DataPacket;

/**
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 */
public class TapMotionEvent extends MotionEvent {
    private int mTapCount;

    public TapMotionEvent(DataPacket packet) {
        super(packet, Type.TAP);
    }

    public int getTapCount() {
        return mTapCount;
    }

    public void setTapCount(int tapCount) {
        mTapCount = tapCount;
    }
}
