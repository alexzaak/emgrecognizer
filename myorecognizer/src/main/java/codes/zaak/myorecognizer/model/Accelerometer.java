package codes.zaak.myorecognizer.model;

/**
 * Created by Alexander Zaak on 08.06.16.
 */
public class Accelerometer {
    private final double xAxis;
    private final double yAxis;
    private final double zAxis;

    public Accelerometer(double xAxis, double yAxis, double zAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.zAxis = zAxis;
    }


    public double getxAxis() {
        return xAxis;
    }



    public double getyAxis() {
        return yAxis;
    }



    public double getzAxis() {
        return zAxis;
    }
}
