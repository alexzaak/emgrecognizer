package codes.zaak.myorecognizer.model;

/**
 * Created by ALexander Zaak on 08.06.16.
 */
public class Orientation {

    private final double xAxis;
    private final double yAxis;
    private final double zAxis;
    private final double wAxis;

    private final double roll;
    private final double pitch;
    private final double yaw;

    private final double rollDegrees;
    private final double pitchDegrees;
    private final double yawDegrees;

    public Orientation(double wAxis, double xAxis, double yAxis, double zAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.zAxis = zAxis;
        this.wAxis = wAxis;

        roll = Math.atan2(2.0 * (wAxis * xAxis + yAxis * zAxis), 1.0 - 2.0 * (xAxis * xAxis + yAxis * yAxis));
        rollDegrees = Math.toDegrees(roll);


        yaw = Math.atan2(2.0 * (wAxis * zAxis + xAxis * yAxis), 1.0 - 2.0 * (yAxis * yAxis + zAxis * zAxis));
        yawDegrees = Math.toDegrees(yaw);

        pitch = Math.asin(2.0 * (wAxis * yAxis - zAxis * xAxis));
        pitchDegrees = Math.toDegrees(pitch);

    }


    public double getxAxis() {
        return xAxis;
    }


    public double getyAxis() {
        return yAxis;
    }


    public double getzAxis() {
        return zAxis;
    }

    public double getwAxis() {
        return wAxis;
    }


    public double getRoll() {
        return roll;
    }

    public double getPitch() {
        return pitch;
    }

    public double getYaw() {
        return yaw;
    }


    public double getRollDegrees() {
        return rollDegrees;
    }

    public double getPitchDegrees() {
        return pitchDegrees;
    }

    public double getYawDegrees() {
        return yawDegrees;
    }
}
