package codes.zaak.myorecognizer;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import codes.zaak.myorecognizer.model.Myo;

/**
 * Created by Alexander Zaak on 21.04.16.
 */
public interface BleManager {





    void startDiscover();

    void startDiscover(final long scanPeriod, final MyoListener.ScannerCallback scannerCallback);

    void stopDiscover(final MyoListener.ScannerCallback scannerCallback);




    ArrayList<MyoController> getMyoList();}
