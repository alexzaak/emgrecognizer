package codes.zaak.myorecognizer.communication;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.UUID;

import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.services.BatteryService;
import codes.zaak.myorecognizer.services.ClassifierService;
import codes.zaak.myorecognizer.services.ControlService;
import codes.zaak.myorecognizer.services.DeviceService;
import codes.zaak.myorecognizer.services.EmgService;
import codes.zaak.myorecognizer.services.GenericService;
import codes.zaak.myorecognizer.services.ImuService;
import codes.zaak.myorecognizer.services.MyoCharacteristic;
import codes.zaak.myorecognizer.services.MyoDescriptor;
import codes.zaak.myorecognizer.services.MyoService;

/**
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 */
public class ReadMessage extends MyoCommunicator {



    private byte[] mValue;

    public ReadMessage(MyoCharacteristic myoCharacteristic, MyoListener.CommunicationCallback callback) {
        this(myoCharacteristic.getServiceUUID(), myoCharacteristic.getCharacteristicUUID(), callback);
    }

    /**
     * See <br/>
     * {@link BatteryService}<br/>
     * {@link ClassifierService}<br/>
     * {@link ControlService}<br/>
     * {@link DeviceService}<br/>
     * {@link EmgService}<br/>
     * {@link GenericService}<br/>
     * {@link ImuService}<br/>
     * <p/>
     *
     * @param serviceUUID        e.g. {@link MyoService#getServiceUUID()} or {@link MyoCharacteristic#getServiceUUID()}
     * @param characteristicUUID e.g. {@link MyoCharacteristic#getCharacteristicUUID()}
     * @param callback           A callback that will return this object after it has been processed
     */
    public ReadMessage(@NonNull UUID serviceUUID, @NonNull UUID characteristicUUID, @Nullable MyoListener.CommunicationCallback callback) {
        super(serviceUUID, characteristicUUID, callback);
    }


    /**
     * The value that has been read.
     * Initialized with NULL and will be send shortly before the callback is triggered.
     * Depending on {@link #getGattStatus()} this can also be NULL after the callback.
     *
     * @return The value read from the bluetooth device. Can be NULL.
     */
    public byte[] getValue() {
        return mValue;
    }

    /**
     * {@link codes.zaak.myorecognizer.MyoGattCallback} will use this method to set the value after the bluetooth transmission returned with {@link android.bluetooth.BluetoothGatt#GATT_SUCCESS}
     * Don't set this yourself.
     *
     */
    public void setValue(byte[] value) {
        mValue = value;
    }

    @Override
    public String toString() {
        return "ReadMsg\n" + "Value: " + Arrays.toString(mValue) + "\n" + super.toString();
    }
}
