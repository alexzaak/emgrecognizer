package codes.zaak.myorecognizer.communication;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.UUID;

import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.services.BatteryService;
import codes.zaak.myorecognizer.services.ClassifierService;
import codes.zaak.myorecognizer.services.ControlService;
import codes.zaak.myorecognizer.services.DeviceService;
import codes.zaak.myorecognizer.services.EmgService;
import codes.zaak.myorecognizer.services.GenericService;
import codes.zaak.myorecognizer.services.ImuService;
import codes.zaak.myorecognizer.services.MyoCharacteristic;
import codes.zaak.myorecognizer.services.MyoDescriptor;
import codes.zaak.myorecognizer.services.MyoService;

import static codes.zaak.myorecognizer.MyoStates.*;

/**
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 */
public class MyoCommunicator {

    private final UUID serviceUUID;
    private final UUID characteristicUUID;
    private final UUID descriptorUUID;
    private final MyoListener.CommunicationCallback callback;
    private int retryCounter = -1;
    private Integer gattStatus = null;
    private CommunicationState mState = CommunicationState.NEW;


    /**
     *  Android Myo library by darken
     * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
     * mHealth - Uniklinik RWTH-Aachen
     *
     * @see <a href="https://github.com/d4rken/myolib">more Information</a>
     *
     * See <br/>
     * {@link BatteryService}<br/>
     * {@link ClassifierService}<br/>
     * {@link ControlService}<br/>
     * {@link DeviceService}<br/>
     * {@link EmgService}<br/>
     * {@link GenericService}<br/>
     * {@link ImuService}<br/>
     * <p/>
     *
     * @param serviceUUID        e.g. {@link MyoService#getServiceUUID()} or {@link MyoCharacteristic#getServiceUUID()}
     * @param characteristicUUID e.g. {@link MyoCharacteristic#getCharacteristicUUID()}
     * @param descriptorUUID     Optional, depending on whether you want to target a descriptor or not.
     *                           See {@link MyoDescriptor}
     * @param callback           A callback that will return this object after it has been processed
     */
    public MyoCommunicator(@NonNull UUID serviceUUID, @NonNull UUID characteristicUUID, @Nullable UUID descriptorUUID, @Nullable MyoListener.CommunicationCallback callback) {
        this.serviceUUID = serviceUUID;
        this.characteristicUUID = characteristicUUID;
        this.descriptorUUID = descriptorUUID;
        this.callback = callback;
    }

    /**
     * Convenience constructor for creating a characteristic only message.<br>
     * See {@link #MyoCommunicator(UUID, UUID, MyoListener.CommunicationCallback)} (UUID, UUID, UUID, Callback)}
     *
     */
    public MyoCommunicator(@NonNull UUID serviceUUID, @NonNull UUID characteristicUUID, @Nullable MyoListener.CommunicationCallback callback) {
        this.serviceUUID = serviceUUID;
        this.characteristicUUID = characteristicUUID;
        this.descriptorUUID = null;
        this.callback = callback;
    }

    /**
     * The current state of this message.
     *
     */
    public CommunicationState getState() {
        return mState;
    }

    /**
     * This will be used by {@link CommunicationState}. Don't call it yourself.
     *
     */
    public void setState(CommunicationState state) {
        mState = state;
    }

    /**
     * The {@link android.bluetooth.BluetoothGatt} status for this transmission.
     * This will be NULL until the message has been send.
     * It will be set shortly before the callback is issued.
     *
     * @return e.g. {@link android.bluetooth.BluetoothGatt#GATT_SUCCESS}
     */
    public Integer getGattStatus() {
        return gattStatus;
    }

    /**
     * Set by {@link codes.zaak.myorecognizer.MyoController} after a message is send, shortly before the callback is triggered.
     * Don't set this yourself.
     *
     */
    public void setGattStatus(Integer gattStatus) {
        this.gattStatus = gattStatus;
    }

    public int decreaseRetryCounter() {
        if (retryCounter > 0)
            retryCounter--;
        return retryCounter;
    }

    public int getRetryCounter() {
        return retryCounter;
    }

    /**
     * How often this message should be retried when:<br>
     * {@link #getGattStatus()} != {@link android.bluetooth.BluetoothGatt#GATT_SUCCESS} <br>
     * after a transmission.
     *
     * @param retryCounter -1 for infinite tries otherwise >=0 tries
     */
    public void setRetryCounter(int retryCounter) {
        retryCounter = retryCounter;
    }

    public UUID getServiceUUID() {
        return serviceUUID;
    }

    public UUID getCharacteristicUUID() {
        return characteristicUUID;
    }

    public UUID getDescriptorUUID() {
        return descriptorUUID;
    }

    public MyoListener.CommunicationCallback getCallback() {
        return callback;
    }

    /**
     * An identifier string for this message. This is not unique.
     * Two messages can have the identifier but as we are transmitting messages in sequence and not in parallel, it is not an issue.
     *
     */
    public String getIdentifier() {
        StringBuilder builder = new StringBuilder();
        builder.append(serviceUUID.toString());
        builder.append(":").append(characteristicUUID.toString());
        if (descriptorUUID != null)
            builder.append(":").append(descriptorUUID.toString());
        return builder.toString();
    }

    @Override
    public String toString() {
        return "Identifier: " + getIdentifier();
    }

    /**
     * A convenience method to generate the identifier based on a {@link BluetoothGattCharacteristic} object.<br>
     * Also see {@link #getIdentifier()}.
     */
    public static String toIdentifier(BluetoothGattCharacteristic characteristic) {
        StringBuilder builder = new StringBuilder();
        builder.append(characteristic.getService().getUuid().toString());
        builder.append(":").append(characteristic.getUuid().toString());
        return builder.toString();
    }

    /**
     * See {@link #toIdentifier(BluetoothGattCharacteristic)}
     */
    public static String toIdentifier(BluetoothGattDescriptor descriptor) {
        StringBuilder builder = new StringBuilder();
        builder.append(descriptor.getCharacteristic().getService().getUuid().toString());
        builder.append(":").append(descriptor.getCharacteristic().getUuid().toString());
        builder.append(":").append(descriptor.getUuid().toString());
        return builder.toString();
    }
}
