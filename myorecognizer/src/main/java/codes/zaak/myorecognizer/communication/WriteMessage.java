package codes.zaak.myorecognizer.communication;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.UUID;

import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.services.BatteryService;
import codes.zaak.myorecognizer.services.ClassifierService;
import codes.zaak.myorecognizer.services.ControlService;
import codes.zaak.myorecognizer.services.DeviceService;
import codes.zaak.myorecognizer.services.EmgService;
import codes.zaak.myorecognizer.services.GenericService;
import codes.zaak.myorecognizer.services.ImuService;
import codes.zaak.myorecognizer.services.MyoCharacteristic;
import codes.zaak.myorecognizer.services.MyoDescriptor;
import codes.zaak.myorecognizer.services.MyoService;

/**
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 */
public class WriteMessage extends MyoCommunicator {


    private final byte[] mData;

    public WriteMessage(MyoCharacteristic characteristic, byte[] data, MyoListener.CommunicationCallback callback) {
        this(characteristic.getServiceUUID(), characteristic.getCharacteristicUUID(), null, data, callback);
    }

    public WriteMessage(MyoDescriptor descriptor, byte[] data, MyoListener.CommunicationCallback callback) {
        this(descriptor.getServiceUUID(), descriptor.getCharacteristicUUID(), descriptor.getDescriptorUUID(), data, callback);
    }

    /**
     * See <br/>
     * {@link BatteryService}<br/>
     * {@link ClassifierService}<br/>
     * {@link ControlService}<br/>
     * {@link DeviceService}<br/>
     * {@link EmgService}<br/>
     * {@link GenericService}<br/>
     * {@link ImuService}<br/>
     * <p/>
     *
     * @param serviceUUID        e.g. {@link MyoService#getServiceUUID()} or {@link MyoCharacteristic#getServiceUUID()}
     * @param characteristicUUID e.g. {@link MyoCharacteristic#getCharacteristicUUID()}
     * @param descriptorUUID     Optional, depending on whether you want to target a descriptor or not.
     *                           See {@link MyoDescriptor}
     * @param callback           A callback that will return this object after it has been processed
     */
    public WriteMessage(@NonNull UUID serviceUUID, @NonNull UUID characteristicUUID, @Nullable UUID descriptorUUID, byte[] data, @Nullable MyoListener.CommunicationCallback callback) {
        super(serviceUUID, characteristicUUID, descriptorUUID, callback);
        mData = data;
    }

    public byte[] getData() {
        return mData;
    }

    @Override
    public String toString() {
        return "WriteMsg\n" + "Data: " + Arrays.toString(mData) + "\n" + super.toString();
    }
}
