package codes.zaak.myorecognizer;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import codes.zaak.myorecognizer.commands.MyoCommander;
import codes.zaak.myorecognizer.commands.MyoCommands;
import codes.zaak.myorecognizer.communication.MyoCommunicator;
import codes.zaak.myorecognizer.communication.ReadMessage;
import codes.zaak.myorecognizer.communication.WriteMessage;
import codes.zaak.myorecognizer.model.Myo;
import codes.zaak.myorecognizer.services.BatteryService;
import codes.zaak.myorecognizer.services.ControlService;
import codes.zaak.myorecognizer.services.DeviceService;
import codes.zaak.myorecognizer.services.GenericService;
import codes.zaak.myorecognizer.utils.ByteReader;

/**
 * Created by Alexander Zaak on 24.04.16.
 */
public class MyoController extends MyoGattCallback {

    private Myo myo;

    private String deviceName;
    private String manufacturerName;
    private String firmware;
    private int batteryLevel = -1;


    private MyoCommands.EmgMode emgMode = MyoCommands.EmgMode.NONE;
    private MyoCommands.ImuMode imuMode = MyoCommands.ImuMode.ALL;
    private MyoCommands.ClassifierMode classifierMode = MyoCommands.ClassifierMode.DISABLED;
    private MyoCommands.SleepMode sleepMode = MyoCommands.SleepMode.NORMAL;


    private static final String TAG = MyoController.class.getName();

    public MyoController(Context context, BluetoothDevice device) {
        super(context, device);
    }


    public Myo getMyo() {
        return myo;
    }

    /**
     * Returns a cached devicename, call  to update it.<br/>
     * Is also updated on successfull {@link #writeDeviceName(String, MyoListener.MyoCommandCallback)} calls.
     */
    public String getDeviceName() {
        return deviceName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public String getFirmware() {
        return firmware;
    }

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public MyoCommands.EmgMode getEmgMode() {
        return emgMode;
    }

    public MyoCommands.ImuMode getImuMode() {
        return imuMode;
    }

    public MyoCommands.ClassifierMode getClassifierMode() {
        return classifierMode;
    }

    public MyoCommands.SleepMode getSleepMode() {
        return sleepMode;
    }

    public void readInfo(final MyoListener.ReadMyoInfoCallback callback) {
        MyoCommunicator msg = new ReadMessage(ControlService.MYOINFO, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator msg) {
                if (msg.getState() == MyoStates.CommunicationState.SUCCESS)
                    myo = new Myo((ReadMessage) msg);
                if (callback != null)
                    callback.onReadMyoInfo(MyoController.this, msg, myo);
            }
        });
        submit(msg);
    }

    /**
     * Sets a new device name, if successful {@link #getDeviceName()} will also be updated.
     *
     * @param newName  make it confirms to bluetooth device name specs
     * @param callback optional
     */
    public void writeDeviceName(final String newName, @Nullable final MyoListener.MyoCommandCallback callback) {
        MyoCommunicator writeMsg = new WriteMessage(GenericService.DEVICE_NAME, newName.getBytes(), new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator communicator) {
                if (communicator.getState() == MyoStates.CommunicationState.SUCCESS) {
                    Log.d(TAG, "Name set to:" + newName);
                    deviceName = newName;
                }
                if (callback != null)
                    callback.onCommandDone(MyoController.this, communicator);
            }
        });
        submit(writeMsg);
    }

    /**
     * Reads the current device name through from the device.<br/>
     * While there also is {@link BluetoothDevice#getName()} it often only returns a short version of the name.
     *
     * @param callback optional
     */
    public void readDeviceName(@Nullable final MyoListener.ReadMyoInfoCallback callback) {
        MyoCommunicator msg = new ReadMessage(GenericService.DEVICE_NAME, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator communicator) {
                if (communicator.getState() == MyoStates.CommunicationState.SUCCESS)
                    deviceName = new String(((ReadMessage) communicator).getValue());
                if (callback != null)
                    callback.onDeviceNameRead(MyoController.this, communicator, deviceName);
            }
        });
        submit(msg);
    }


    /**
     * @param callback optional
     */
    public void readManufacturerName(@Nullable final MyoListener.ReadMyoInfoCallback callback) {
        MyoCommunicator msg = new ReadMessage(DeviceService.MANUFACTURER_NAME, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator communicator) {
                if (communicator.getState() == MyoStates.CommunicationState.SUCCESS)
                    manufacturerName = new String(((ReadMessage) communicator).getValue());
                if (callback != null)
                    callback.onManufacturerNameRead(MyoController.this, communicator, manufacturerName);
            }
        });
        submit(msg);
    }

    /**
     * @param callback optional
     */
    public void readFirmware(@Nullable final MyoListener.ReadMyoInfoCallback callback) {
        MyoCommunicator msg = new ReadMessage(ControlService.FIRMWARE_VERSION, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator msg) {
                if (msg.getState() == MyoStates.CommunicationState.SUCCESS) {
                    ByteReader byteHelper = new ByteReader(((ReadMessage) msg).getValue());
                    firmware = String.format("v%d.%d.%d - %d", byteHelper.getUInt16(), byteHelper.getUInt16(), byteHelper.getUInt16(), byteHelper.getUInt16());
                }
                if (callback != null)
                    callback.onFirmwareRead(MyoController.this, msg, firmware);
            }
        });
        submit(msg);
    }

    /**
     * @param callback optional
     */
    public void readBatteryLevel(@Nullable final MyoListener.BatteryCallback callback) {
        MyoCommunicator msg = new ReadMessage(BatteryService.BATTERY_LEVEL, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator msg) {
                if (msg.getState() == MyoStates.CommunicationState.SUCCESS)
                    batteryLevel = ((ReadMessage) msg).getValue()[0];
                if (callback != null)
                    callback.onBatteryLevelRead(MyoController.this, msg, batteryLevel);
            }
        });
        submit(msg);
    }

    /**
     * Sets the Myo's sensor modes. It's not possible to set these individually.
     *
     * @param emgMode        emgMode
     * @param imuMode        imuMode
     * @param classifierMode classifier on/off
     * @param callback       optional
     */
    public void writeMode(final MyoCommands.EmgMode emgMode, final MyoCommands.ImuMode imuMode, final MyoCommands.ClassifierMode classifierMode, @Nullable final MyoListener.MyoCommandCallback callback) {
        byte[] cmd = MyoCommander.buildSensorModeCmd(emgMode, imuMode, classifierMode);
        MyoCommunicator writeMsg = new WriteMessage(ControlService.COMMAND, cmd, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator msg) {
                if (msg.getState() == MyoStates.CommunicationState.SUCCESS) {
                    Log.d(TAG, "Mode set. (EMG:" + emgMode.name() + ", INU:" + imuMode.name() + ", CLASSIFIER:" + classifierMode.name() + ")");
                    MyoController.this.emgMode = emgMode;
                    MyoController.this.imuMode = imuMode;
                    MyoController.this.classifierMode = classifierMode;
                }
                if (callback != null)
                    callback.onCommandDone(MyoController.this, msg);

                getCurrentConnectionState();

            }
        });
        submit(writeMsg);
    }

    /**
     * Sets the Myo's sleep mode. If the Myo disconnects, it's sleep mode will return to the default state.
     *
     * @param sleepMode default {@link MyoCommands.SleepMode}
     * @param callback  optional
     */
    public void writeSleepMode(final MyoCommands.SleepMode sleepMode, @Nullable final MyoListener.MyoCommandCallback callback) {
        byte[] cmd = MyoCommander.buildSleepModeCmd(sleepMode);
        MyoCommunicator writeMsg = new WriteMessage(ControlService.COMMAND, cmd, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator msg) {
                if (msg.getState() == MyoStates.CommunicationState.SUCCESS)
                    MyoController.this.sleepMode = sleepMode;
                if (callback != null)
                    callback.onCommandDone(MyoController.this, msg);

                getCurrentConnectionState();
            }
        });
        submit(writeMsg);
    }


    /**
     * Puts the Myo into a deep-sleep state.
     * Moving the Myo will no longer wake it up.
     * Waking it up requires plugging the usb cable into it.
     *
     * @param callback optional
     */
    public void writeDeepSleep(@Nullable final MyoListener.MyoCommandCallback callback) {
        byte[] cmd = MyoCommander.buildDeepSleepCmd();
        MyoCommunicator writeMsg = new WriteMessage(ControlService.COMMAND, cmd, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator msg) {
                if (msg.getState() == MyoStates.CommunicationState.SUCCESS)
                    Log.d(TAG, "DeepSleep!");
                if (callback != null)
                    callback.onCommandDone(MyoController.this, msg);

                getCurrentConnectionState();
            }
        });
        submit(writeMsg);
    }

    public void writeVibrate(MyoCommands.VibrateType vibrateType, @Nullable final MyoListener.MyoCommandCallback callback) {
        byte[] cmd = MyoCommander.buildVibrateCmd(vibrateType);
        MyoCommunicator writeMsg = new WriteMessage(ControlService.COMMAND, cmd, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator msg) {
                if (msg.getState() == MyoStates.CommunicationState.SUCCESS)
                    Log.d(TAG, "Vibrated!");
                if (callback != null)
                    callback.onCommandDone(MyoController.this, msg);
            }
        });
        submit(writeMsg);
    }

    /**
     * @param unlockType LOCK, TIMED or HOLD
     * @param callback   optional
     */
    public void writeUnlock(final MyoCommands.UnlockType unlockType, @Nullable final MyoListener.MyoCommandCallback callback) {
        byte[] cmd = MyoCommander.buildSetUnlockModeCmd(unlockType);
        final MyoCommunicator writeMsg = new WriteMessage(ControlService.COMMAND, cmd, new MyoListener.CommunicationCallback() {
            @Override
            public void onResult(MyoCommunicator msg) {
                if (msg.getState() == MyoStates.CommunicationState.SUCCESS)
                    Log.d(TAG, "SleepMode set: " + unlockType.name());
                if (callback != null)
                    callback.onCommandDone(MyoController.this, msg);

                getCurrentConnectionState();
            }
        });
        submit(writeMsg);
    }

}
