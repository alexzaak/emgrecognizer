package codes.zaak.myorecognizer.gestures;

import android.content.Context;

import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoController;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.model.Accelerometer;
import codes.zaak.myorecognizer.model.Gyroscope;
import codes.zaak.myorecognizer.model.Orientation;
import codes.zaak.myorecognizer.processor.emg.EmgData;
import codes.zaak.myorecognizer.processor.emg.EmgProcessor;
import codes.zaak.myorecognizer.processor.imu.ImuProcessor;

/**
 * Created by user on 08.06.16.
 */
public class CustomGestureController implements MyoListener.EmgDataListener, MyoListener.CustomGesturesListener {

    private EmgProcessor emgProcessor;


    private GestureSave gestureRecord;

    private GestureDetect gestureDetection;

    private MyoStates.GestureStatus gestureStatus = MyoStates.GestureStatus.STOPPED;

    private Gestures.CustomGestures lastDetectedGesture = Gestures.CustomGestures.UNDEFINED;

    private Gestures.CustomGestures gestureToStore = Gestures.CustomGestures.UNDEFINED;

    public CustomGestureController(Context context, MyoController myoController, MyoListener.CustomGesturesListener customGesturesListener) {


        lastDetectedGesture = Gestures.CustomGestures.UNDEFINED;
        emgProcessor = new EmgProcessor();
        emgProcessor.addListener(this);
        myoController.addProcessor(emgProcessor);

        gestureDetection = new GestureDetect(context, customGesturesListener, this);
        gestureDetection.getStoredData();

        gestureRecord = new GestureSave(context, customGesturesListener);
    }

    public void addListener() {
        emgProcessor.addListener(this);
    }

    public void removeListener() {
        emgProcessor.removeDataListener(this);
    }

    public void setGestureStatus(Gestures.CustomGestures gesture, MyoStates.GestureStatus status) {

        if (status.equals(MyoStates.GestureStatus.RECORDING)) {
            gestureToStore = gesture;
        }

        if (status.equals(MyoStates.GestureStatus.DETECTING)) {
            gestureDetection.getStoredData();
        }

        if (status.equals(MyoStates.GestureStatus.CLEAR)) {
            this.gestureStatus = MyoStates.GestureStatus.STOPPED;
            gestureDetection.clear();
            gestureRecord.clear();

        }

        this.gestureStatus = status;
    }

    public synchronized Gestures.CustomGestures getLastDetectedGesture() {
        return lastDetectedGesture;
    }


    @Override
    public void onNewEmgData(EmgData emgData) {
        if (gestureStatus.equals(MyoStates.GestureStatus.DETECTING)) {
            gestureDetection.getDetectGesture(emgData.getData());
        }

        if (gestureStatus.equals(MyoStates.GestureStatus.RECORDING)) {
            gestureRecord.addData(gestureToStore, emgData.getData());
        }
    }

    @Override
    public void onGestureDetected(long timeStamp, Gestures.CustomGestures gesture) {
        lastDetectedGesture = gesture;
    }

    @Override
    public void onGestureProfileLoaded(long timeStamp, Gestures.ProfileStatus status, int gestureCount) {

        if (status == Gestures.ProfileStatus.SUCCESS) {
            this.gestureStatus = MyoStates.GestureStatus.DETECTING;
        }
    }

    @Override
    public void onGestureRecordingStatusChanged(long timeStamp, Gestures.CustomGestures currentGesture, Gestures.GestureSaveStatus status) {
        if (status == Gestures.GestureSaveStatus.SUCCESS || status == Gestures.GestureSaveStatus.ERROR) {
            this.gestureStatus = MyoStates.GestureStatus.DETECTING;
        }
    }

    @Override
    public void onGestureStored(long timeStamp, Gestures.CustomGestures savedGesture, Gestures.GestureSaveStatus success) {
        if (success == Gestures.GestureSaveStatus.SUCCESS || success == Gestures.GestureSaveStatus.ERROR) {
            this.gestureStatus = MyoStates.GestureStatus.DETECTING;
        }
    }

    @Override
    public void onCheckGestureDistance(long l, Gestures.CustomGestures gesture, Gestures.CustomGestures gestureToStore, Gestures.GestureSaveStatus error) {

    }
}
