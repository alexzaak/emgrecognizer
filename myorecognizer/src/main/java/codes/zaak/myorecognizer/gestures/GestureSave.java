package codes.zaak.myorecognizer.gestures;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.model.Orientation;
import codes.zaak.myorecognizer.processor.emg.EmgData;
import codes.zaak.myorecognizer.utils.Const;
import codes.zaak.myorecognizer.utils.DistanceCalculator;
import codes.zaak.myorecognizer.utils.EmgCharacteristicData;
import codes.zaak.myorecognizer.utils.PrefManager;

/**
 * Created by user on 28.04.16.
 */
public class GestureSave {

    private final static int COMPARE_NUM = Gestures.CustomGestures.values().length - 1;
    private final static int SAVE_DATA_LENGTH = 5;
    private final static int AVERAGING_LENGTH = 10;

    private final static Double THRESHOLD_MIN = 0.95;
    private static final String TAG = GestureSave.class.getName();

    private HashMap<String, List<Orientation>> orientationMap = new HashMap<>();
    private List<Orientation> orientationList = new ArrayList<>();


    private List<EmgCharacteristicData> rawDataList = new ArrayList<>();
    private List<EmgData> maxDataList = new ArrayList<>();

    private Gestures.CustomGestures gestureToStore = Gestures.CustomGestures.UNDEFINED;

    private int dataCounter = 0;

    private MyoListener.CustomGesturesListener recordingListener;
    private PrefManager prefManager;

    public GestureSave(Context context, MyoListener.CustomGesturesListener listener) {

        prefManager = new PrefManager(context, Const.CUSTOM_PROFILE);


        setListener(listener);

        if (prefManager.prefsExists()) {
            this.recordingListener.onGestureRecordingStatusChanged(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.SUCCESS);
        } else {
            //  prefManager.setPrefInitSuccess();
            this.recordingListener.onGestureRecordingStatusChanged(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.READY);
        }
    }

    public void setListener(MyoListener.CustomGesturesListener listener) {
        this.recordingListener = listener;
        this.recordingListener.onGestureRecordingStatusChanged(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.READY);
    }


    public void clear() {
        dataCounter = 0;
        gestureToStore = Gestures.CustomGestures.UNDEFINED;
        rawDataList.clear();
        maxDataList.clear();
    }

    public void addOrientationData(Gestures.CustomGestures gestureToStore, Orientation orientationData) {
        orientationList.add(orientationData);
        orientationMap.put(gestureToStore.name(), orientationList);
    }

    public void storeOrientationData(Gestures.CustomGestures gestureToStore) {

        if (orientationList.isEmpty()) {
            return;
        }

        double roll = 0;
        double pitch = 0;
        double yaw = 0;


        for (Orientation orientation : orientationList) {
            roll += orientation.getRollDegrees();
            pitch += orientation.getPitchDegrees();
            yaw += orientation.getYaw();


        }


        roll = roll / orientationList.size();
        pitch = pitch / orientationList.size();
        yaw = yaw / orientationList.size();

        Log.i("CALIBRATE", "Gesture " + gestureToStore.name() + " Roll: " + roll + "\nPitch: " + pitch + "\nYaw: " + yaw);
        prefManager.storeOrientation(gestureToStore.name(), roll, pitch, yaw);

        orientationList.clear();
        orientationMap.clear();
    }

    public void addData(Gestures.CustomGestures gestureToStore, byte[] rawEmgData) {

        if (gestureToStore != null) {
            this.gestureToStore = gestureToStore;
            addData(rawEmgData);
        }
    }

    /**
     * @param rawEmgData 16 EMG-Values
     */
    public void addData(byte[] rawEmgData) {

        if (this.gestureToStore == Gestures.CustomGestures.UNDEFINED) {
            this.recordingListener.onGestureStored(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.ERROR);
            return;
        }

        this.recordingListener.onGestureRecordingStatusChanged(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.SAVING);

        rawDataList.add(new EmgCharacteristicData(rawEmgData));

        dataCounter++;
        if (dataCounter % SAVE_DATA_LENGTH == 0) {
            EmgData dataMax = new EmgData(this.gestureToStore, null, System.currentTimeMillis(), rawEmgData);
            int count = 0;

            for (EmgCharacteristicData aRawDataList : rawDataList) {

                EmgData emg8Temp = aRawDataList.getEmgFiltered();
                if (count == 0) {
                    dataMax = emg8Temp;
                } else {
                    for (int index = 0; index < 8; index++) {
                        if (emg8Temp.getEmgData(index) > dataMax.getEmgData(index)) {
                            dataMax.setEmgData(index, emg8Temp.getEmgData(index));
                        }
                    }
                }
                count++;
            }
            if (rawDataList.size() < SAVE_DATA_LENGTH) {
                this.recordingListener.onGestureRecordingStatusChanged(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.ERROR);
                Log.e("GestureDetect", "Small rawData : " + rawDataList.size());
            }
            maxDataList.add(dataMax);
            rawDataList = new ArrayList<>();
        }
        if (dataCounter == SAVE_DATA_LENGTH * AVERAGING_LENGTH) {
            // saveState = SaveState.Not_Saved;
            EmgData finalEmgData = makeCompareData();

            if (checkGestureDistance(finalEmgData)) {
                gestureCount(finalEmgData.getLine());

            } else {
                this.gestureToStore = Gestures.CustomGestures.UNDEFINED;
                this.recordingListener.onGestureRecordingStatusChanged(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.ERROR);
                this.recordingListener.onGestureStored(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.ERROR);

            }
            dataCounter = 0;

        }
    }

    private boolean checkGestureDistance(EmgData finalEmgData) {

        MyoStates.AlgorithmType algorithmType =  prefManager.getAlgorithmType();

        List<EmgData> dataList = prefManager.getGestureList(Gestures.CustomGestures.values());

        for (EmgData storedData : dataList) {
            double distance = DistanceCalculator.calculateDistance(algorithmType, finalEmgData, storedData);
            Log.i(TAG, "Algorithm type: " + algorithmType.name());
            Log.i(TAG, String.format("Stored Gesture: %s \nNew Gesture: %s \nDistance:  %.5f", storedData.getName(), this.gestureToStore.name(), distance));

            if (distance > THRESHOLD_MIN && !this.gestureToStore.name().equals(storedData.getName())) {
                this.recordingListener.onCheckGestureDistance(System.currentTimeMillis(), storedData.getGesture(), this.gestureToStore, Gestures.GestureSaveStatus.ERROR);
                return false;
            }
        }

        return true;
    }


    private EmgData makeCompareData() {
        EmgData tempData = new EmgData(this.gestureToStore, null, System.currentTimeMillis(), null);

        // Get each Max EMG-elements of maxDataList
/*        int count = 0;

        for (EmgData emg8Temp : maxDataList) {
            if (count == 0) {
                tempData = emg8Temp;
            } else {
                for (int index = 0; index < 8; index++) {
                    if (emg8Temp.getEmgData(index) > tempData.getEmgData(index)) {
                        tempData.setEmgData(index, emg8Temp.getEmgData(index));
                    }
                }
            }
            count++;
        }*/

        // Averaging EMG-elements of maxDataList
        int count = 0;
        for (EmgData emg8Temp : maxDataList) {
            if (count == 0) {
                tempData = emg8Temp;
            } else {
                for (int i_element = 0; i_element < 8; i_element++) {
                    tempData.setEmgData(i_element, tempData.getEmgData(i_element) + emg8Temp.getEmgData(i_element));
                }
            }
            count++;
        }
        for (int i_element = 0; i_element < 8; i_element++) {
            tempData.setEmgData(i_element, tempData.getEmgData(i_element) / maxDataList.size());
        }

        if (maxDataList.size() < AVERAGING_LENGTH) {
            this.recordingListener.onGestureRecordingStatusChanged(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.ERROR);
            Log.e("GestureDetect", "Small aveData : " + maxDataList.size());
        }

        maxDataList = new ArrayList<>();
        return tempData;
    }

    private void gestureCount(String finalEmgData) {

        // if (gestureCounter == COMPARE_NUM) {

        // gestureCounter = 0;


        prefManager.storeGesture(this.gestureToStore.name(), finalEmgData);
        prefManager.setPrefInitSuccess();
        this.recordingListener.onGestureRecordingStatusChanged(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.SUCCESS);
        this.recordingListener.onGestureStored(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.SUCCESS);


     /*   if (gestureCounter == COMPARE_NUM) {
        this.recordingListener.onGestureRecordingStatusChanged(System.currentTimeMillis(), this.gestureToStore, Gestures.GestureSaveStatus.READY);
         }*/
    }

}

