package codes.zaak.myorecognizer.gestures;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoListener;
import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.processor.emg.EmgData;
import codes.zaak.myorecognizer.utils.Const;
import codes.zaak.myorecognizer.utils.DistanceCalculator;
import codes.zaak.myorecognizer.utils.EmgCharacteristicData;
import codes.zaak.myorecognizer.utils.NumberSmoother;
import codes.zaak.myorecognizer.utils.PrefManager;

/**
 * Created by user on 22.04.16.
 */
public class GestureDetect {

    private static final int DIFFERENTIAL_RATE = 50;
    private static final int DATA_RECORD_AMOUNT = 10;
    private final static Double THRESHOLD = 0.9;
    private int countEmgData = 0;
    List<EmgData> compareDataList = new ArrayList<>();
    private double detectedDistance;
    private int detectNumber;

    private final MyoStates.AlgorithmType algorithmType;

    private static int COMPARE_NUMBER = Gestures.CustomGestures.values().length - 1;

    private NumberSmoother numberSmoother = new NumberSmoother();

    private EmgData emgMaxData;

    private List<MyoListener.CustomGesturesListener> gesturesListenerList = new ArrayList<>();

    private PrefManager prefManager;

    public GestureDetect(Context context, MyoListener.CustomGesturesListener... gesturesListenerList) {
        prefManager = new PrefManager(context, Const.CUSTOM_PROFILE);
        algorithmType = prefManager.getAlgorithmType();

        for (MyoListener.CustomGesturesListener listener : gesturesListenerList) {
            this.gesturesListenerList.add(listener);
        }

    }

    public void clear() {

        compareDataList.clear();
        countEmgData = 0;
        prefManager.clear();
    }

    public void getStoredData() {
        compareDataList.clear();
        compareDataList.addAll(prefManager.getGestureList(Gestures.CustomGestures.values()));
        COMPARE_NUMBER = compareDataList.size();
        if (COMPARE_NUMBER > 0) {
            notifyProfileListener(Gestures.ProfileStatus.SUCCESS);
        } else {
            notifyProfileListener(Gestures.ProfileStatus.ERROR);
        }
    }

    public Double getThreshold() {
        return THRESHOLD;
    }


    public void setCompareDataList(List<EmgData> compareDataList) {
        Log.d("GestureDetector", "CompareList: " + compareDataList.size());
        this.compareDataList.addAll(compareDataList);
    }

    public float getRollByGesture(Gestures.CustomGestures gesture) {
        return prefManager.getRoll(gesture);
    }

    public float getPitchByGesture(Gestures.CustomGestures gesture) {
        return prefManager.getPitch(gesture);
    }

    public float getYawByGesture(Gestures.CustomGestures gesture) {
        return prefManager.getYaw(gesture);
    }


    public void getDetectGesture(byte[] data) {
        EmgData streamData = new EmgCharacteristicData(data).getEmgFiltered();


        countEmgData++;
        if (countEmgData == 1) {
            emgMaxData = streamData;
        } else {
            for (int index = 0; index < 8; index++) {
                if (streamData.getEmgData(index) > emgMaxData.getEmgData(index)) {
                    emgMaxData.setEmgData(index, streamData.getEmgData(index));
                }
            }
            if (countEmgData == DATA_RECORD_AMOUNT) {
                detectedDistance = getThreshold();
                detectNumber = -1;

                // Log.d("GestureDetect", "Count: " + COMPARE_NUMBER);
                for (int i_gesture = 0; i_gesture < COMPARE_NUMBER; i_gesture++) {
                    EmgData compData = compareDataList.get(i_gesture);


                    double distance = DistanceCalculator.calculateDistance(algorithmType, emgMaxData, compData);

                    if (detectedDistance < distance) {

                        detectedDistance = distance;

                        detectNumber = i_gesture;
                    }
                }
                numberSmoother.addArray(detectNumber);
                countEmgData = 0;
            }
        }
        int smoothed = numberSmoother.getSmoothingNumber();
        Gestures.CustomGestures detectedGesture = Gestures.getGesture(smoothed);
        notifyDetectListener(detectedGesture);
    }

    private void notifyProfileListener(Gestures.ProfileStatus status) {

        for (MyoListener.CustomGesturesListener listener : this.gesturesListenerList) {
            listener.onGestureProfileLoaded(System.currentTimeMillis(), status, COMPARE_NUMBER);
        }
    }


    private void notifyDetectListener(Gestures.CustomGestures gesture) {

        for (MyoListener.CustomGesturesListener listener : this.gesturesListenerList) {
            listener.onGestureDetected(System.currentTimeMillis(), gesture);
        }
    }
}
