package codes.zaak.myorecognizer;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import codes.zaak.myorecognizer.commands.MyoCommands;
import codes.zaak.myorecognizer.gestures.CustomGestureController;
import codes.zaak.myorecognizer.processor.classifier.ClassifierProcessor;
import codes.zaak.myorecognizer.processor.imu.ImuProcessor;
import codes.zaak.myorecognizer.processor.imu.MotionProcessor;
import codes.zaak.myorecognizer.utils.Const;
import codes.zaak.myorecognizer.utils.PrefManager;

/**
 * Created by Alexander Zaak on 09.06.16.
 */
public class MyoHub implements MyoListener.ConnectionListener {

    private static final String TAG = MyoHub.class.getName();
    private ClassifierProcessor classifierProcessor;
    private ImuProcessor imuProcessor;
    private MotionProcessor motionProcessor;

    private CustomGestureController customGestureController;

    private MyoController myoController;

    private MyoStates.ConnectionState currentConnectionState = MyoStates.ConnectionState.DISCONNECTED;

    public MyoHub(MyoController myoController, MyoListener.ConnectionListener connectionListener, MyoStates.ConnectionSpeed connectionSpeed) {

        this.myoController = myoController;

        this.myoController.addConnectionListener(this);
        this.myoController.addConnectionListener(connectionListener);
        this.myoController.setConnectionSpeed(connectionSpeed);
        this.myoController.connect();



    }


    public void setup(MyoCommands.EmgMode emgMode, MyoCommands.ImuMode imuMode, MyoCommands.ClassifierMode classifierMode, MyoListener.MyoCommandCallback commandCallback) {
        this.myoController.writeSleepMode(MyoCommands.SleepMode.NEVER, commandCallback);
        this.myoController.writeMode(emgMode, imuMode, classifierMode, commandCallback);
        this.myoController.writeUnlock(MyoCommands.UnlockType.HOLD, commandCallback);

    }

    public void addConnectionListener(MyoListener.ConnectionListener connectionListener) {

        if (this.myoController != null) {
            this.myoController.addConnectionListener(connectionListener);
        }
    }

    public void removeConnectionListener(MyoListener.ConnectionListener connectionListener) {
        if (this.myoController != null) {
            this.myoController.removeConnectionListener(connectionListener);
        }
    }

    public void setClassifierListener(MyoListener.ClassifierEventListener classifierListener) {
        classifierProcessor = new ClassifierProcessor();
        classifierProcessor.addListener(classifierListener);
        myoController.addProcessor(classifierProcessor);
    }

    public void removeClassifierListener(MyoListener.ClassifierEventListener classifierListener) {
        classifierProcessor.removeDataListener(classifierListener);
    }

    public void setImuListener(MyoListener.ImuDataListener imuListener) {
        imuProcessor = new ImuProcessor();
        imuProcessor.addListener(imuListener);
        myoController.addProcessor(imuProcessor);
    }

    public void removeImuListener(MyoListener.ImuDataListener imuListener) {
        imuProcessor.removeDataListener(imuListener);
    }

    public void setMotionListener(MyoListener.MotionEventListener motionListener) {
        motionProcessor = new MotionProcessor();
        motionProcessor.addListener(motionListener);
        myoController.addProcessor(motionProcessor);
    }

    public void removeMotionListener(MyoListener.MotionEventListener motionListener) {
        motionProcessor.removeDataListener(motionListener);
    }

    public void setCustomGestureListener(Context context, MyoListener.CustomGesturesListener customGesturesListener) {
        customGestureController = new CustomGestureController(context, myoController, customGesturesListener);

    }


    public void setBatteryListener(final MyoListener.BatteryCallback batteryListener) {

        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                myoController.readBatteryLevel(batteryListener);
            }
        };

        Timer timer = new Timer();
        timer.schedule(task, 1000, 60000);


    }

    public CustomGestureController getCustomGestureController() {
        return customGestureController;
    }

    public MyoController getMyoController() {
        return myoController;
    }

    public void vibrate(MyoCommands.VibrateType length) {
        this.myoController.writeVibrate(length, null);
    }

    public void unlock(MyoCommands.UnlockType unlockType) {
        this.myoController.writeUnlock(unlockType, null);
    }

    public void setAlgorithmType(Context context, MyoStates.AlgorithmType type) {
        PrefManager prefManager = new PrefManager(context, Const.CUSTOM_PROFILE);
        prefManager.setAlgorithmType(type);
    }


    public void shutdown() {
        if (this.myoController != null) {
            this.myoController.writeSleepMode(MyoCommands.SleepMode.NORMAL, null);
            myoController.writeMode(MyoCommands.EmgMode.NONE, MyoCommands.ImuMode.NONE, MyoCommands.ClassifierMode.DISABLED, null);
            this.myoController.disconnect();

        }
    }

    public void notifyConnectionStateChanged() {
        this.myoController.getCurrentConnectionState();
    }

    public MyoStates.ConnectionState getConnectionState() {
        return this.currentConnectionState;
    }

    @Override
    public void onConnectionStateChanged(MyoGattCallback myoCallback, MyoStates.ConnectionState state) {
        myoCallback.getConnectionState();
        Log.i(TAG, state.name());
        this.currentConnectionState = state;
    }
}
