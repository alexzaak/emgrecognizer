package codes.zaak.myorecognizer;

import java.util.List;

import codes.zaak.myorecognizer.communication.MyoCommunicator;
import codes.zaak.myorecognizer.model.Accelerometer;
import codes.zaak.myorecognizer.model.Gyroscope;
import codes.zaak.myorecognizer.model.Myo;
import codes.zaak.myorecognizer.model.Orientation;
import codes.zaak.myorecognizer.processor.BaseProcessor;
import codes.zaak.myorecognizer.processor.classifier.ClassifierEvent;
import codes.zaak.myorecognizer.processor.emg.EmgData;
import codes.zaak.myorecognizer.processor.imu.ImuData;
import codes.zaak.myorecognizer.processor.imu.MotionEvent;

/**
 * Created by Alexander Zaak on 24.04.16.
 */
public class MyoListener {

    public interface ScannerCallback {
        void onScanFinished(List<MyoController> myoList);

        void onScanFailed(MyoStates.ScanError error);
    }

    public interface ConnectionListener {
        void onConnectionStateChanged(MyoGattCallback myoCallback, MyoStates.ConnectionState state);
    }

    public interface CommunicationCallback {
        void onResult(MyoCommunicator communicator);
    }

    public interface ClassifierEventListener extends BaseProcessor.DataListener {

        void onClassifierEvent(ClassifierEvent classifierEvent);

    }

    public interface EmgDataListener extends BaseProcessor.DataListener {
        void onNewEmgData(EmgData emgData);
    }

    public interface ImuDataListener extends BaseProcessor.DataListener {
        void onOrientationChanged(Orientation orientationData);

        void onAccelerationChanged(Accelerometer accelerometerData);

        void onGyroscopeChanged(Gyroscope gyroscopeData);

    }

    public interface MotionEventListener extends BaseProcessor.DataListener {
        void onMotionEvent(MotionEvent motionEvent);
    }

    public interface ReadMyoInfoCallback {
        void onReadMyoInfo(MyoController myoController, MyoCommunicator msg, Myo myo);

        void onDeviceNameRead(MyoController myoController, MyoCommunicator communicator, String deviceName);

        void onManufacturerNameRead(MyoController myoController, MyoCommunicator communicator, String manufacturer);

        void onFirmwareRead(MyoController myoController, MyoCommunicator communicator, String version);
    }

    public interface BatteryCallback {
        void onBatteryLevelRead(MyoController myoController, MyoCommunicator communicator, int batteryLevel);
    }

    /**
     * Call back for submitted {@link MyoCommunicator} objects
     */
    public interface MyoCommandCallback {
        /**
         * @param myoController the myo this was run on
         * @param communicator  the same object that was submitted, it now contains information from the transmission
         */
        void onCommandDone(MyoController myoController, MyoCommunicator communicator);
    }


    public interface CustomGesturesListener {
        void onGestureDetected(long timeStamp, Gestures.CustomGestures gesture);

        void onGestureProfileLoaded(long timeStamp, Gestures.ProfileStatus status, int gestureCount);

        void onGestureRecordingStatusChanged(long timeStamp, Gestures.CustomGestures currentStoringGesture, Gestures.GestureSaveStatus status);

        void onGestureStored(long timeStamp, Gestures.CustomGestures savedGesture, Gestures.GestureSaveStatus success);

        void onCheckGestureDistance(long l, Gestures.CustomGestures gesture, Gestures.CustomGestures gestureToStore, Gestures.GestureSaveStatus error);
    }

}
