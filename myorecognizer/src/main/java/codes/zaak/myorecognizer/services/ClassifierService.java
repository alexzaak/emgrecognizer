package codes.zaak.myorecognizer.services;

import java.util.UUID;

/**
 *  Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen
 *
 * @see <a href="https://github.com/d4rken/myolib">more Information</a>
 *
 * Describes accessible Classifier services/characteristics/descriptors
 */
public class ClassifierService {
    protected static final UUID SERVICE_ID = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0003));

    protected static final UUID CLASSIFIER_EVENT_CHARACTERISTIC = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0103));

    public static final MyoService SERVICE = new MyoService(SERVICE_ID);

    public static UUID getServiceUUID() {
        return SERVICE.getServiceUUID();
    }

    /**
     * Indicate-only
     */
    public static final MyoCharacteristic CLASSIFIER_EVENT = new MyoCharacteristic(SERVICE, CLASSIFIER_EVENT_CHARACTERISTIC, "Classifier Event");

    public static final MyoDescriptor CLASSIFIER_EVENT_DESCRIPTOR = new MyoDescriptor(CLASSIFIER_EVENT, MyoDescriptor.CLIENT_CHARACTERISTIC_CONFIG);
}
