package codes.zaak.myorecognizer.services;

import java.util.UUID;

/**
 *  Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen
 *
 * @see <a href="https://github.com/d4rken/myolib">more Information</a>
 * <p/>
 * Describes accessible IMU services/characteristics/descriptors
 */
public class ImuService {
    protected static final UUID SERVICE_ID = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0002));

    protected static final UUID IMU_DATA_CHARACTERISTIC = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0402));
    protected static final UUID MOTION_EVENT_CHARACTERISTIC = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0502));

    public static final MyoService SERVICE = new MyoService(SERVICE_ID);

    public static UUID getServiceUUID() {
        return SERVICE.getServiceUUID();
    }

    /**
     * Notify-only
     */
    public static final MyoCharacteristic IMU_DATA = new MyoCharacteristic(SERVICE, IMU_DATA_CHARACTERISTIC, "Imu Data");
    public static final MyoDescriptor IMU_DATA_DESCRIPTOR = new MyoDescriptor(IMU_DATA, MyoCharacteristic.CLIENT_CHARACTERISTIC_CONFIG);
    /**
     * Indicate-only
     */
    public static final MyoCharacteristic MOTION_EVENT = new MyoCharacteristic(SERVICE, MOTION_EVENT_CHARACTERISTIC, "Motion Event");
    public static final MyoDescriptor MOTION_EVENT_DESCRIPTOR = new MyoDescriptor(MOTION_EVENT, MyoCharacteristic.CLIENT_CHARACTERISTIC_CONFIG);
}