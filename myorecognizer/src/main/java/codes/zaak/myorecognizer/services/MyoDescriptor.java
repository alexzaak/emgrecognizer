package codes.zaak.myorecognizer.services;

import java.util.UUID;

/**
 *  Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen
 *
 * @see <a href="https://github.com/d4rken/myolib">more Information</a>
 *
 * Holds the UUID for a specific descriptor
 */
public class MyoDescriptor extends MyoCharacteristic {
    private final UUID mDescriptor;
    private final MyoCharacteristic mCharacteristic;

    public MyoDescriptor(MyoCharacteristic characteristic, UUID descriptor) {
        super(characteristic.getService(), characteristic.getCharacteristicUUID(), characteristic.getName());
        mCharacteristic = characteristic;
        mDescriptor = descriptor;
    }

    public MyoCharacteristic getCharacteristic() {
        return mCharacteristic;
    }

    public UUID getDescriptorUUID() {
        return mDescriptor;
    }
}
