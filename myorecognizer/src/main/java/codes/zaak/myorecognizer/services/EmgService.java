package codes.zaak.myorecognizer.services;

import java.util.UUID;

/**
 *  Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen
 *
 * @see <a href="https://github.com/d4rken/myolib">more Information</a>
 * Describes accessible EMG services/characteristics/descriptors
 */
public class EmgService {
    protected static final UUID SERVICE_UUID = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0005));

    protected static final UUID EMGDATA0_CHARACTERISTIC_UUID = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0105));
    protected static final UUID EMGDATA1_CHARACTERISTIC_UUID = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0205));
    protected static final UUID EMGDATA2_CHARACTERISTIC_UUID = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0305));
    protected static final UUID EMGDATA3_CHARACTERISTIC_UUID = UUID.fromString(String.format(MyoService.MYO_SERVICE_BASE_UUID, 0x0405));

    public static final MyoService SERVICE = new MyoService(SERVICE_UUID);

    /**
     * Notify-only
     */
    public static final MyoCharacteristic EMG_DATA_0 = new MyoCharacteristic(SERVICE, EMGDATA0_CHARACTERISTIC_UUID, "Emg0 Data");
    public static final MyoDescriptor EMG_DATA_0_DESCRIPTOR = new MyoDescriptor(EMG_DATA_0, MyoCharacteristic.CLIENT_CHARACTERISTIC_CONFIG);
    /**
     * Notify-only
     */
    public static final MyoCharacteristic EMG_DATA_1 = new MyoCharacteristic(SERVICE, EMGDATA1_CHARACTERISTIC_UUID, "Emg1 Data");
    public static final MyoDescriptor EMG_DATA_1_DESCRIPTOR = new MyoDescriptor(EMG_DATA_1, MyoCharacteristic.CLIENT_CHARACTERISTIC_CONFIG);
    /**
     * Notify-only
     */
    public static final MyoCharacteristic EMG_DATA_2 = new MyoCharacteristic(SERVICE, EMGDATA2_CHARACTERISTIC_UUID, "Emg2 Data");
    public static final MyoDescriptor EMG_DATA_2_DESCRIPTOR = new MyoDescriptor(EMG_DATA_2, MyoCharacteristic.CLIENT_CHARACTERISTIC_CONFIG);
    /**
     * Notify-only
     */
    public static final MyoCharacteristic EMG_DATA_3 = new MyoCharacteristic(SERVICE, EMGDATA3_CHARACTERISTIC_UUID, "Emg3 Data");
    public static final MyoDescriptor EMG_DATA_3_DESCRIPTOR = new MyoDescriptor(EMG_DATA_3, MyoCharacteristic.CLIENT_CHARACTERISTIC_CONFIG);

    public static UUID getServiceUUID() {
        return SERVICE.getServiceUUID();
    }

}
