package codes.zaak.myorecognizer.commands;

/**
 * Created by Alexander Zaak on 24.04.16.
 */
public class MyoCommander {


    public static byte[] buildDeepSleepCmd() {
        return new byte[]{
                0x04,
                0
        };
    }

    public static byte[] buildSetUnlockModeCmd(MyoCommands.UnlockType unlockType) {
        return new byte[]{
                0x0a,
                1,
                unlockType.getByte()
        };
    }

    public static byte[] buildVibrateCmd(MyoCommands.VibrateType vibrateType) {
        return new byte[]{
                0x03, // vibrate command
                1, // payload size
                vibrateType.getByte() // vibration type
        };
    }


    public static byte[] buildSleepModeCmd(MyoCommands.SleepMode sleepMode) {
        return new byte[]{
                0x09,
                1,
                sleepMode.getByte()
        };
    }

    public static byte[] buildSensorModeCmd(MyoCommands.EmgMode emgMode, MyoCommands.ImuMode imuMode, MyoCommands.ClassifierMode classifierMode) {
        return new byte[]{
                0x01, // mode command
                3, // payload size
                emgMode.getByte(),
                imuMode.getByte(),
                classifierMode.getByte()
        };
    }
}
