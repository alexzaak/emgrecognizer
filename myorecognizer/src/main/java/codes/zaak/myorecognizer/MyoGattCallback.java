package codes.zaak.myorecognizer;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import codes.zaak.myorecognizer.communication.MyoCommunicator;
import codes.zaak.myorecognizer.communication.ReadMessage;
import codes.zaak.myorecognizer.communication.WriteMessage;
import codes.zaak.myorecognizer.processor.BaseDataPacket;
import codes.zaak.myorecognizer.processor.BaseProcessor;
import codes.zaak.myorecognizer.processor.Processor;
import codes.zaak.myorecognizer.services.BatteryService;
import codes.zaak.myorecognizer.services.ClassifierService;
import codes.zaak.myorecognizer.services.ControlService;
import codes.zaak.myorecognizer.services.EmgService;
import codes.zaak.myorecognizer.services.ImuService;
import codes.zaak.myorecognizer.services.MyoDescriptor;

/**
 * Android Myo library by darken
 * Matthias Urhahn (matthias.urhahn@rwth-aachen.de)
 * mHealth - Uniklinik RWTH-Aachen.
 *
 * This is the base class for all Myo communication.
 * It wraps a {@link BluetoothGatt} object and supplies methods to easy communication.
 * Communication is encapsulated via {@link MyoCommunicator} and {@link #submit(MyoCommunicator)}.
 */
public class MyoGattCallback extends BluetoothGattCallback {

    private final BlockingQueue<MyoCommunicator> dispatchQueue = new LinkedBlockingQueue<>();
    private final Map<String, MyoCommunicator> communicationCallbackMap = new HashMap<>();
    private final Map<UUID, List<Processor>> subscriptionMap = new HashMap<>();

    private final Object threadControl = new Object();
    private volatile boolean isRunning = false;
    private final Context context;
    private final BluetoothDevice device;
    private final List<MyoListener.ConnectionListener> connectionListenerList = new ArrayList<>();
    private volatile MyoStates.ConnectionState connectionState = MyoStates.ConnectionState.DISCONNECTED;
    private final Semaphore waitToken = new Semaphore(0);
    private MyoStates.ConnectionSpeed connectionSpeed = MyoStates.ConnectionSpeed.BALANCED;
    private BluetoothGatt bluetoothGatt;

    private volatile long timeoutSendQueue = 250;

    private static final String TAG = MyoGattCallback.class.getName();


    public MyoGattCallback(Context context, BluetoothDevice device) {
        this.context = context;
        this.device = device;
    }

    public String getDeviceAddress() {
        return getBluetoothDevice().getAddress();
    }

    public void setConnectionSpeed(@NonNull MyoStates.ConnectionSpeed speed) {

        connectionSpeed = speed;
    }

    public MyoStates.ConnectionSpeed getConnectionSpeed() {
        return connectionSpeed;
    }

    public BluetoothDevice getBluetoothDevice() {
        return device;
    }

    public MyoStates.ConnectionState getConnectionState() {
        return connectionState;
    }

    public void addConnectionListener(MyoListener.ConnectionListener listener) {
        connectionListenerList.add(listener);
    }

    public void removeConnectionListener(MyoListener.ConnectionListener listener) {
        connectionListenerList.remove(listener);
    }

    /**
     * Submits a new message to the dispatcher of this device.
     * It will be put at the end of the queue and once it reaches the front.
     * Messages are sequentially as otherwise instruction can be lost.<br/>
     * If dispatcher of this Myo is not yet running, {@link #connect()} will be called.
     * It will be taken care of that the Myo is ready before any transmission attempt will be made.
     * Don't alter the message object after submitting it
     *
     * @param communicator A {@link WriteMessage} or {@link ReadMessage}
     */
    public void submit(@NonNull MyoCommunicator communicator) {
        dispatchQueue.add(communicator);
        synchronized (threadControl) {
            if (!isRunning) {
                connect();
            }
        }
    }

    /**
     * "Starts this Myo"<br/>
     * Launches the innerloop that dispatches {@link MyoCommunicator}.
     * This loop will wait until {@link #getConnectionState()} changes to {@link MyoStates.ConnectionState#CONNECTED}
     * <p>
     * Calling this multiple times has no effect.
     */
    public void connect() {
        synchronized (threadControl) {
            if (isRunning) {
                return;
            } else {
                Log.d(TAG, "Connecting to " + device.getName());
                waitToken.drainPermits();
                isRunning = true;
                new Thread(mLoop).start();
            }
        }
    }

    /**
     * Disconnects the bluetooth connection and stops the dispatcher loop.
     */
    public void disconnect() {
        synchronized (threadControl) {
            if (!isRunning) {
                return;
            } else {
                isRunning = false;
                waitToken.release();

                Log.d(TAG, "Disconnecting from " + device.getName());
            }
        }
    }

    public void getCurrentConnectionState() {
        for (MyoListener.ConnectionListener listener : connectionListenerList) {
            listener.onConnectionStateChanged(this, connectionState);
        }
    }

    /**
     * Adds a Processor object to this Myo, make sure it is unique.
     */
    public void addProcessor(Processor processor) {
        for (UUID subscriptionTarget : processor.getSubscriptions()) {
            List<Processor> subscriberList = subscriptionMap.get(subscriptionTarget);
            if (subscriberList == null) {
                subscriberList = new ArrayList<>();
                subscriptionMap.put(subscriptionTarget, subscriberList);
            } else {
                if (subscriberList.contains(processor))
                    continue;
            }
            subscriberList.add(processor);
        }
        processor.onAdded();
    }

    public void removeProcessor(BaseProcessor processor) {
        processor.onRemoved();
        for (UUID subscriptionTarget : processor.getSubscriptions()) {
            List<Processor> subscriberList = subscriptionMap.get(subscriptionTarget);
            if (subscriberList != null)
                subscriberList.remove(processor);
        }
    }

    /**
     * Whether the Dispatcher is running.<br/>
     * NOT if the Myo device is connected.
     * Use {@link #getConnectionState()} for that.<br/>
     * The dispatcher can be running, but the Myo device temporarily disconnected.
     *
     * @return true if the dispatcher is running
     */
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        if (newState == BluetoothProfile.STATE_CONNECTING) {
            connectionState = MyoStates.ConnectionState.CONNECTING;
        } else if (newState == BluetoothProfile.STATE_CONNECTED) {
            connectionState = MyoStates.ConnectionState.CONNECTED;
            Log.d(TAG, "Device connected, discovering services...");
            gatt.discoverServices();
        } else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
            connectionState = MyoStates.ConnectionState.DISCONNECTING;
            waitToken.drainPermits();
        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            connectionState = MyoStates.ConnectionState.DISCONNECTED;
        } else {
            throw new RuntimeException("Unknown connection state");
        }
        Log.d(TAG, "status:" + status + ", newState:" + connectionState.name());
        for (MyoListener.ConnectionListener listener : connectionListenerList) {
            listener.onConnectionStateChanged(this, connectionState);
        }
        super.onConnectionStateChange(gatt, status, newState);
    }


    /**
     * Checks available Myo services and enables EMG and IMU characteristic notifications.
     */
    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        if (status != BluetoothGatt.GATT_SUCCESS) {
            Log.w(TAG, "Service discovered failed!");
            return;
        }

        BluetoothGattService controlService = bluetoothGatt.getService(ControlService.getServiceUUID());
        if (controlService != null) {
            Log.d(TAG, "Service Control: available");
            BluetoothGattCharacteristic myoInfo = controlService.getCharacteristic(ControlService.MYOINFO.getCharacteristicUUID());
            Log.d(TAG, "Characteristic MyoInfo: " + (myoInfo != null ? "available" : "unavailable"));
            BluetoothGattCharacteristic fimwareInfo = controlService.getCharacteristic(ControlService.FIRMWARE_VERSION.getCharacteristicUUID());
            Log.d(TAG, "Characteristic FirmwareInfo: " + (fimwareInfo != null ? "available" : "unavailable"));
            BluetoothGattCharacteristic commandCharacteristic = controlService.getCharacteristic(ControlService.COMMAND.getCharacteristicUUID());
            Log.d(TAG, "Characteristic Command: " + (commandCharacteristic != null ? "available" : "unavailable"));
        } else {
            Log.w(TAG, "Service Control: unavailable");
        }

        BluetoothGattService emgService = bluetoothGatt.getService(EmgService.SERVICE.getServiceUUID());
        if (emgService != null) {
            Log.d(TAG, "Service EMG: available");
            enableNotifications(emgService, EmgService.EMG_DATA_0_DESCRIPTOR);
            enableNotifications(emgService, EmgService.EMG_DATA_1_DESCRIPTOR);
            enableNotifications(emgService, EmgService.EMG_DATA_2_DESCRIPTOR);
            enableNotifications(emgService, EmgService.EMG_DATA_3_DESCRIPTOR);
        } else {
            Log.w(TAG, "Service EMG: unavailable");
        }

        BluetoothGattService imuService = bluetoothGatt.getService(ImuService.getServiceUUID());
        if (imuService != null) {
            Log.d(TAG, "Service IMU: available");
            enableNotifications(imuService, ImuService.IMU_DATA_DESCRIPTOR);
            enableIndication(imuService, ImuService.MOTION_EVENT_DESCRIPTOR);
        } else {
            Log.w(TAG, "Service IMU: unavailable");
        }

        BluetoothGattService classifierService = bluetoothGatt.getService(ClassifierService.getServiceUUID());
        if (classifierService != null) {
            Log.d(TAG, "Service Classifier: available");
            enableIndication(classifierService, ClassifierService.CLASSIFIER_EVENT_DESCRIPTOR);
        } else {
            Log.w(TAG, "Service Classifier: unavailable");
        }

        BluetoothGattService batteryService = bluetoothGatt.getService(BatteryService.getServiceUUID());
        if (batteryService != null) {
            Log.d(TAG, "Service Battery: available");
        } else {
            Log.w(TAG, "Service Battery: unavailable");
        }
        super.onServicesDiscovered(gatt, status);

        Log.d(TAG, "Services discovered.");
        waitToken.release();

    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int gattStatus) {
        ReadMessage msg = (ReadMessage) communicationCallbackMap.remove(MyoCommunicator.toIdentifier(characteristic));
        waitToken.release();

        msg.setGattStatus(gattStatus);
        if (gattStatus == BluetoothGatt.GATT_SUCCESS) {
            Log.v(TAG, "rtt: " + (System.currentTimeMillis() - dispatchTime) + "ms | SUCCESS | " + msg.toString());
            msg.setState(MyoStates.CommunicationState.SUCCESS);
            msg.setValue(characteristic.getValue());
            if (msg.getCallback() != null)
                msg.getCallback().onResult(msg);
        } else {
            Log.w(TAG, "rtt: " + (System.currentTimeMillis() - dispatchTime) + "ms | ERROR(" + gattStatus + ") | " + msg.toString());
            msg.setState(MyoStates.CommunicationState.ERROR);
            if (msg.getRetryCounter() == 0) {
                if (msg.getCallback() != null)
                    msg.getCallback().onResult(msg);
            } else {
                msg.decreaseRetryCounter();
                submit(msg);
            }
        }
        super.onCharacteristicRead(gatt, characteristic, gattStatus);
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int gattStatus) {
        WriteMessage msg = (WriteMessage) communicationCallbackMap.remove(MyoCommunicator.toIdentifier(characteristic));
        waitToken.release();

        msg.setGattStatus(gattStatus);
        if (gattStatus == BluetoothGatt.GATT_SUCCESS) {
            Log.v(TAG, "rtt: " + (System.currentTimeMillis() - dispatchTime) + "ms | SUCCESS | " + msg.toString());
            msg.setState(MyoStates.CommunicationState.SUCCESS);
            if (msg.getCallback() != null)
                msg.getCallback().onResult(msg);
        } else {
            Log.w(TAG, "rtt: " + (System.currentTimeMillis() - dispatchTime) + "ms | ERROR(" + gattStatus + ") | " + msg.toString());
            msg.setState(MyoStates.CommunicationState.ERROR);
            if (msg.getRetryCounter() == 0) {
                if (msg.getCallback() != null)
                    msg.getCallback().onResult(msg);
            } else {
                msg.decreaseRetryCounter();
                submit(msg);
            }
        }
        super.onCharacteristicWrite(gatt, characteristic, gattStatus);
    }


    @Override
    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int gattStatus) {
        ReadMessage msg = (ReadMessage) communicationCallbackMap.remove(MyoCommunicator.toIdentifier(descriptor));
        waitToken.release();

        msg.setGattStatus(gattStatus);
        if (gattStatus == BluetoothGatt.GATT_SUCCESS) {
            Log.v(TAG, "rtt: " + (System.currentTimeMillis() - dispatchTime) + "ms | SUCCESS | " + msg.toString());
            msg.setState(MyoStates.CommunicationState.SUCCESS);
            msg.setValue(descriptor.getValue());
            if (msg.getCallback() != null)
                msg.getCallback().onResult(msg);
        } else {
            Log.w(TAG, "rtt: " + (System.currentTimeMillis() - dispatchTime) + "ms | ERROR(" + gattStatus + ") | " + msg.toString());
            msg.setState(MyoStates.CommunicationState.ERROR);
            if (msg.getRetryCounter() == 0) {
                if (msg.getCallback() != null)
                    msg.getCallback().onResult(msg);
            } else {
                msg.decreaseRetryCounter();
                submit(msg);
            }
        }
        super.onDescriptorRead(gatt, descriptor, gattStatus);
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int gattStatus) {
        WriteMessage msg = (WriteMessage) communicationCallbackMap.remove(MyoCommunicator.toIdentifier(descriptor));
        waitToken.release();

        msg.setGattStatus(gattStatus);
        if (gattStatus == BluetoothGatt.GATT_SUCCESS) {
            Log.v(TAG, "rtt: " + (System.currentTimeMillis() - dispatchTime) + "ms | SUCCESS | " + msg.toString());
            msg.setState(MyoStates.CommunicationState.SUCCESS);
            if (msg.getCallback() != null)
                msg.getCallback().onResult(msg);
        } else {
            Log.w(TAG, "rtt: " + (System.currentTimeMillis() - dispatchTime) + "ms | ERROR(" + gattStatus + ") | " + msg.toString());
            msg.setState(MyoStates.CommunicationState.ERROR);
            if (msg.getRetryCounter() == 0) {
                if (msg.getCallback() != null)
                    msg.getCallback().onResult(msg);
            } else {
                msg.decreaseRetryCounter();
                submit(msg);
            }
        }
        super.onDescriptorWrite(gatt, descriptor, gattStatus);
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        BaseDataPacket packet = new BaseDataPacket(gatt, characteristic);
        List<Processor> subscribers = subscriptionMap.get(characteristic.getUuid());
        if (subscribers != null) {
            for (Processor subscriber : subscribers)
                subscriber.submit(packet);
        }
        super.onCharacteristicChanged(gatt, characteristic);
    }

    private Runnable mLoop = new Runnable() {
        private int mPriority = MyoStates.ConnectionSpeed.BALANCED.getPriority();

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_DEFAULT);
            bluetoothGatt = device.connectGatt(context, true, MyoGattCallback.this);
            while (isRunning) {
                if (connectionState != MyoStates.ConnectionState.CONNECTED) {

                    continue;
                }

                if (getConnectionSpeed().getPriority() != mPriority) {
                    mPriority = getConnectionSpeed().getPriority();
                    bluetoothGatt.requestConnectionPriority(mPriority);
                }


                try {
                    if (timeoutSendQueue == -1) {
                        waitToken.acquire();
                    } else {
                        if (!waitToken.tryAcquire(timeoutSendQueue, TimeUnit.MILLISECONDS)) {
                            Log.w(TAG, "Lost packet!");


                        }

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (!isRunning) {
                    break;
                }

                MyoCommunicator communicator = dispatchQueue.poll();
                if (communicator != null) {
                    internalSend(communicator);
                } else {
                    waitToken.release();
                }
            }

            bluetoothGatt.disconnect();
            bluetoothGatt.close();
            bluetoothGatt = null;
        }
    };

    private void disconnectBtGatt() {
        bluetoothGatt.disconnect();
        bluetoothGatt.close();
        bluetoothGatt = null;
    }

    private long dispatchTime = 0;

    private void internalSend(MyoCommunicator communication) {
        BluetoothGattService gattService = bluetoothGatt.getService(communication.getServiceUUID());
        if (gattService == null) {
            Log.w(TAG, "BluetoothGattService unavailable!: " + communication.toString());
            return;
        }
        BluetoothGattCharacteristic gattChar = gattService.getCharacteristic(communication.getCharacteristicUUID());
        if (gattChar == null) {
            Log.w(TAG, "BluetoothGattCharacteristic unavailable!: " + communication.toString());
            return;
        }

        dispatchTime = System.currentTimeMillis();
        if (communication.getDescriptorUUID() != null) {
            BluetoothGattDescriptor gattDesc = gattChar.getDescriptor(communication.getDescriptorUUID());
            if (gattDesc == null) {
                Log.w(TAG, "BluetoothGattDescriptor unavailable!: " + communication.toString());
                return;
            }
            communicationCallbackMap.put(communication.getIdentifier(), communication);
            if (communication instanceof WriteMessage) {
                gattDesc.setValue(((WriteMessage) communication).getData());
                bluetoothGatt.writeDescriptor(gattDesc);
            } else {
                bluetoothGatt.readDescriptor(gattDesc);
            }
        } else {
            communicationCallbackMap.put(communication.getIdentifier(), communication);
            if (communication instanceof WriteMessage) {
                gattChar.setValue(((WriteMessage) communication).getData());
                bluetoothGatt.writeCharacteristic(gattChar);
            } else {
                bluetoothGatt.readCharacteristic(gattChar);
            }
        }
        Log.v(TAG, "Processed: " + communication.getIdentifier());
    }


    private void enableNotifications(BluetoothGattService service, final MyoDescriptor descriptor) {
        BluetoothGattCharacteristic classifier = service.getCharacteristic(descriptor.getCharacteristicUUID());
        if (classifier != null && bluetoothGatt.setCharacteristicNotification(classifier, true)) {
            WriteMessage message = new WriteMessage(descriptor,
                    BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE,
                    new MyoListener.CommunicationCallback() {
                        @Override
                        public void onResult(MyoCommunicator msg) {
                            Log.d(TAG, "Notification '" + descriptor.getName() + "' enabled");
                        }
                    });
            submit(message);
        }
    }

    private void enableIndication(BluetoothGattService service, final MyoDescriptor descriptor) {
        BluetoothGattCharacteristic classifier = service.getCharacteristic(descriptor.getCharacteristicUUID());
        if (classifier != null && bluetoothGatt.setCharacteristicNotification(classifier, true)) {
            WriteMessage msg = new WriteMessage(descriptor,
                    BluetoothGattDescriptor.ENABLE_INDICATION_VALUE,
                    new MyoListener.CommunicationCallback() {
                        @Override
                        public void onResult(MyoCommunicator msg) {
                            Log.d(TAG, "Indication '" + descriptor.getName() + "' enabled");
                        }
                    });
            submit(msg);
        }
    }

}
