package codes.zaak.myorecognizer.utils;

import android.util.Log;

import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.processor.emg.EmgData;

/**
 * Created by Alexander Zaak on 28.06.16.
 */
public class DistanceCalculator {

    private static final String TAG = DistanceCalculator.class.getName();

    // 2 vectors distance divided from each vectors norm.
    private static double distanceCalculation(EmgData streamData, EmgData compareData) {

        double currentDataNorm = streamData.getNorm();
        double compareDataNorm = compareData.getNorm();
        double euclideanDistance = streamData.getDistanceFrom(compareData);


        double distance = euclideanDistance / (currentDataNorm * compareDataNorm);
        Log.i(TAG, String.format(" stream norm: %.5f  compare Norm:  %.5f", currentDataNorm, compareDataNorm));
        Log.i(TAG, String.format("Abstand:  %.5f \nDistance:  %.5f", euclideanDistance, distance));
        return distance;
    }

    // Mathematical [sin] value of 2 vectors' inner angle.
    private static double distanceCalculationSinus(EmgData streamData, EmgData compareData) {

        double currentDataNorm = streamData.getNorm();
        double compareDataNorm = compareData.getNorm();
        double innerProduction = streamData.getInnerProductionTo(compareData);


        double distance = streamData.getInnerProductionTo(compareData) / (streamData.getNorm() * compareData.getNorm());
        Log.i(TAG, String.format("Inner Produktion: %.5f stream norm: %.5f  compare Norm:  %.5f", innerProduction, currentDataNorm, compareDataNorm));
        Log.i(TAG, String.format("Stored Gesture: %s \nNew Gesture: %s \nDistance:  %.5f", compareData.getName(), streamData.getName(), distance));
        return distance;
    }

    // Mathematical [cos] value of 2 vectors' inner angle from low of cosines.
    private static double distanceCalculationCosinus(EmgData streamData, EmgData compareData) {
        double streamNorm = streamData.getNorm();
        double compareNorm = compareData.getNorm();

        double euclideanDistance = streamData.getDistanceFrom(compareData);

        double distance = (Math.pow(streamNorm, 2.0) + Math.pow(compareNorm, 2.0) - Math.pow(euclideanDistance, 2.0)) / (streamNorm * compareNorm * 2);
        return distance;
    }

    public static double calculateDistance(MyoStates.AlgorithmType algorithmType, EmgData emgMaxData, EmgData compData) {
        switch (algorithmType) {
            case TWO_VECTOR_DISTANCE:
                return distanceCalculation(emgMaxData, compData);
            case MATHEMATICAL_SINUS_DISTANCE:
                return distanceCalculationSinus(emgMaxData, compData);
            case MATHEMATICAL_COSINUS_DISTANCE:
                return distanceCalculationCosinus(emgMaxData, compData);
        }
        return 0;
    }
}
