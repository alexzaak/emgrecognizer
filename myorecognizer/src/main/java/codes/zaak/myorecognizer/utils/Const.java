package codes.zaak.myorecognizer.utils;

/**
 * Created by Alexander Zaak on 02.05.16.
 */
public class Const {

    public static final String PREFS_INITIALIZED = "prefs_initialized";
    public static final String CUSTOM_PROFILE = "custom_profile";
    public static final String ROLL = "_roll";
    public static final String PITCH = "_pitch";
    public static final String YAW = "_yaw";
    public static final int REQUEST_ENABLE_BT = 100;
    public static final String PREFS_AUTOMATIC_REFINEMENT = "pref_automatic_refinement";

    public static final String PREFS_ALGORITHM_TYPE = "pref_algorithm_type";
}
