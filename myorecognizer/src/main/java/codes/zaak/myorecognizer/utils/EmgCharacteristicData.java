package codes.zaak.myorecognizer.utils;

import java.util.ArrayList;

import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.processor.emg.EmgData;


/**
 * Created by Alexander Zaak on 28.04.16.
 */
public class EmgCharacteristicData {
    private final ByteReader emgData;
    private final byte[] byteData;

    public EmgCharacteristicData(byte[] byteData) {
        emgData =  new ByteReader(byteData);
        this.byteData = byteData;
    }

    public EmgCharacteristicData(ByteReader byteReader){
        emgData = byteReader;
        this.byteData = new byte[8];
    }

    public String getLine() {
        StringBuilder return_SB = new StringBuilder();
        for (int i_emg_num = 0; i_emg_num < 16; i_emg_num++) {
            return_SB.append(String.format("%d,", emgData.getByte()));
        }
        return return_SB.toString();
    }

    public EmgData getEmgFiltered() {

        EmgData emg8Data_max = new EmgData(Gestures.CustomGestures.UNDEFINED, null, System.currentTimeMillis(), this.byteData);
        ArrayList<Double> tempEmgData = new ArrayList<>();
        for (int i_emg_num = 0; i_emg_num < 16; i_emg_num++) {
            double temp = emgData.getByte();
            tempEmgData.add(temp);
        }
        for (int i_emg8 = 0; i_emg8 < 8; i_emg8++) {
            if (Math.abs(tempEmgData.get(i_emg8)) < Math.abs(tempEmgData.get(i_emg8 + 8))){
                emg8Data_max.addEmgData(Math.abs(tempEmgData.get(i_emg8 + 8)));
            } else {
                emg8Data_max.addEmgData(Math.abs(tempEmgData.get(i_emg8)));
            }
        }
        return emg8Data_max;
    }

}
