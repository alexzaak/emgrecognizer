package codes.zaak.myorecognizer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import codes.zaak.myorecognizer.Gestures;
import codes.zaak.myorecognizer.MyoStates;
import codes.zaak.myorecognizer.processor.emg.EmgData;

/**
 * Created by Alexander Zaak on 02.05.16.
 */
public class PrefManager {

    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    private SharedPreferences pref;

    // Constructor
    public PrefManager(Context context, String prefName) {

        pref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public boolean prefsExists() {
        return pref.contains(Const.PREFS_INITIALIZED);
    }

    public void setPrefInitSuccess() {
        editor.putBoolean(Const.PREFS_INITIALIZED, true);
        editor.commit();
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }

    public void storeGesture(String gestureId, String emgData) {
        editor.putString(gestureId, emgData);
        editor.commit();
    }

    public List<EmgData> getGestureList(Gestures.CustomGestures[] gesturesIds) {

        List<EmgData> emgDataList = new ArrayList<>();
        for (Gestures.CustomGestures gestureId : gesturesIds) {
            String emgData = pref.getString(gestureId.name(), null);
            if (emgData == null) {
                continue;
            }
            EmgData data = new EmgData(gestureId, null, System.currentTimeMillis(), null);
            data.setLine(emgData);

            emgDataList.add(data);
        }

        return emgDataList;

    }

    public void storeOrientation(String gestureId, double roll, double pitch, double yaw) {
        editor.putFloat(gestureId + Const.ROLL, (float) roll);
        editor.putFloat(gestureId + Const.PITCH, (float) pitch);
        editor.putFloat(gestureId + Const.YAW, (float) yaw);
        editor.commit();
    }

    public float getRoll(Gestures.CustomGestures gesture) {
        return pref.getFloat(gesture.name() + Const.ROLL, 0);
    }

    public float getPitch(Gestures.CustomGestures gesture) {
        return pref.getFloat(gesture.name() + Const.PITCH, 0);
    }

    public float getYaw(Gestures.CustomGestures gesture) {
        return pref.getFloat(gesture.name() + Const.YAW, 0);
    }

    public void setRefinement(boolean refinement) {
        editor.putBoolean(Const.PREFS_AUTOMATIC_REFINEMENT, refinement);
        editor.commit();
    }

    public boolean isRefinement() {
        return pref.getBoolean(Const.PREFS_AUTOMATIC_REFINEMENT, true);
    }

    public void setAlgorithmType(MyoStates.AlgorithmType type) {
        editor.putInt(Const.PREFS_ALGORITHM_TYPE, type.ordinal());
        editor.commit();
    }

    public MyoStates.AlgorithmType getAlgorithmType() {
        int type = pref.getInt(Const.PREFS_ALGORITHM_TYPE, 0);
        return MyoStates.AlgorithmType.valueOf(type);
    }
}
