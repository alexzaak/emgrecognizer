package codes.zaak.myorecognizer;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothProfile;
import android.os.Build;
import android.util.Log;

/**
 * Created by Alexander Zaak on 24.04.16.
 */
public class MyoStates {

    /**
     * The state of this device, relates to {@link BluetoothProfile#STATE_CONNECTED} etc.
     */
    public enum ConnectionState {
        CONNECTING, CONNECTED, DISCONNECTING, DISCONNECTED
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public enum ConnectionSpeed {
        /**
         * Saves battery power but reducs the data rate.<br>
         * About ~50 packets/s.
         */
        BATTERY_CONSERVING(BluetoothGatt.CONNECTION_PRIORITY_LOW_POWER),
        /**
         * Balance between battery saving and data rate.<br>
         * About 84 packets/s.
         */
        BALANCED(BluetoothGatt.CONNECTION_PRIORITY_BALANCED),
        /**
         * Maximum performance, causes high battery drain.<br>
         * Data rates of 450+ packets/s
         */
        HIGH(BluetoothGatt.CONNECTION_PRIORITY_HIGH);

        private final int mPriority;

        ConnectionSpeed(int priority) {
            mPriority = priority;
        }

        public int getPriority() {
            return mPriority;
        }
    }

    /**
     * Values reflecting state of this {@link codes.zaak.myorecognizer.communication.MyoCommunicator}
     */
    public enum CommunicationState {
        /**
         * Message has not been submitted yet via {@link MyoGattCallback#submit(codes.zaak.myorecognizer.communication.MyoCommunicator)}.
         */
        NEW,
        /**
         * Message was send and possibly confirmed.
         */
        SUCCESS,
        /**
         * The message failed to be transmitted.
         * This includes failure to submit, timeouts, running out of retries.
         */
        ERROR
    }

    public enum GestureStatus {

        RECORDING,

        CLEAR, STOPPED, DETECTING
    }

    public enum AlgorithmType {
        TWO_VECTOR_DISTANCE,

        MATHEMATICAL_SINUS_DISTANCE,

        MATHEMATICAL_COSINUS_DISTANCE;


        private static final String TAG = MyoStates.class.getName();

        public static AlgorithmType valueOf(int type) {
            Log.i(TAG, "Type " + type);

            if (type == MATHEMATICAL_SINUS_DISTANCE.ordinal()) {
                return MATHEMATICAL_COSINUS_DISTANCE;
            }

            if (type == MATHEMATICAL_COSINUS_DISTANCE.ordinal()) {
                return MATHEMATICAL_COSINUS_DISTANCE;
            }
            return MATHEMATICAL_COSINUS_DISTANCE;
        }
    }

    public enum ScanError {
        BLUETOOTH_DISCONNECTED

    }
}
