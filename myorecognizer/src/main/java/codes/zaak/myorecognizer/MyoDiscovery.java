package codes.zaak.myorecognizer;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import codes.zaak.myorecognizer.model.Myo;
import codes.zaak.myorecognizer.services.ControlService;
import codes.zaak.myorecognizer.utils.ByteReader;
import codes.zaak.myorecognizer.utils.Const;

/**
 * Created by Alexander Zaak on 24.04.16.
 */
public class MyoDiscovery implements BleManager {

    private final Context context;
    private final BluetoothAdapter bluetoothAdapter;
    private final Map<String, MyoController> mDeviceMap = new HashMap<>();
    private final Map<String, MyoController> mScanMap = new HashMap<>();
    private final Handler handler;
    private BluetoothLeScanner bluetoothLeScanner;
    private BluetoothGatt bluetoothGatt;
    private ScanSettings settings;
    private List<ScanFilter> filters = new ArrayList<ScanFilter>();

    private Activity activity;


    public MyoDiscovery(Activity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        BluetoothManager bluetoothManager = (BluetoothManager) this.context.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        handler = new Handler();

        if (Build.VERSION.SDK_INT >= 21) {
            bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            filters.clear();
        }

    }


    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            if (result == null || result.getScanRecord() == null) {
                return;
            }
            byte[] scanRecord = result.getScanRecord().getBytes();


            BluetoothDevice device = result.getDevice();
            List<AdRecord> adRecords = AdRecord.parseScanRecord(scanRecord);
            UUID uuid = null;
            for (AdRecord adRecord : adRecords) {
                // TYPE_UUID128_INC
                if (adRecord.getType() == 0x6) {
                    uuid = new ByteReader(adRecord.getData()).getUUID();
                    break;
                }
            }
            if (ControlService.getServiceUUID().equals(uuid)) {
                if (!mScanMap.containsKey(device.getAddress())) {
                    MyoController myoController = mDeviceMap.get(device.getAddress());
                    if (myoController == null) {
                        myoController = new MyoController(context, device);
                        mDeviceMap.put(device.getAddress(), myoController);
                    }
                    mScanMap.put(device.getAddress(), myoController);
                }
            }

        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {

        }

        @Override
        public void onScanFailed(int errorCode) {

        }
    };


    @Override
    public void startDiscover() {
        if (isBtEnabled()) {
            bluetoothLeScanner.stopScan(scanCallback);
        }

    }

    @Override
    public void startDiscover(long scanPeriod, final MyoListener.ScannerCallback scannerCallback) {


        if (!isBtEnabled()) {
            scannerCallback.onScanFailed(MyoStates.ScanError.BLUETOOTH_DISCONNECTED);
            return;
        }


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                bluetoothLeScanner.stopScan(scanCallback);
                scannerCallback.onScanFinished(new ArrayList<>(mDeviceMap.values()));
            }
        }, scanPeriod);
        bluetoothLeScanner.startScan(filters, settings, scanCallback);
    }

    @Override
    public void stopDiscover(final MyoListener.ScannerCallback scannerCallback) {

        if (!isBtEnabled()) {
            scannerCallback.onScanFailed(MyoStates.ScanError.BLUETOOTH_DISCONNECTED);
            return;
        }

        bluetoothLeScanner.stopScan(scanCallback);
        scannerCallback.onScanFinished(new ArrayList<>(mDeviceMap.values()));
    }


    @Override
    public ArrayList<MyoController> getMyoList() {
        return new ArrayList<>(mDeviceMap.values());
    }

    private boolean isBtEnabled() {

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            this.activity.startActivityForResult(enableBtIntent, Const.REQUEST_ENABLE_BT);
            return false;
        } else {
            return true;
        }
    }
}
