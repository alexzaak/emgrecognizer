package codes.zaak.myorecognizer;

/**
 * Created by Alexander Zaak on 28.04.16.
 */
public class Gestures {

    public enum CustomGestures {
        GESTURE_0, GESTURE_1, GESTURE_2, GESTURE_3, GESTURE_4, GESTURE_5, UNDEFINED
    }

    public static CustomGestures getGesture(int index) {
        switch (index) {
            case 0:
                return CustomGestures.GESTURE_0;
            case 1:
                return CustomGestures.GESTURE_1;
            case 2:
                return CustomGestures.GESTURE_2;
            case 3:
                return CustomGestures.GESTURE_3;
            case 4:
                return CustomGestures.GESTURE_4;
            case 5:
                return CustomGestures.GESTURE_5;
            default:
                return CustomGestures.UNDEFINED;
        }
    }

    public enum GestureSaveStatus {
        READY, SUCCESS, SAVING, GESTURE_EXISTS_ALREADY, ERROR
    }

    public enum ProfileStatus {
        SUCCESS,
        ERROR

    }
}
